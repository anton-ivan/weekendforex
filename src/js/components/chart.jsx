'use strict';

import Actions from '../actions/actions';
import Store from '../stores/chartstore';
import SelectedTradeStore from '../stores/tradetablestore';
import IndicatorStore from '../stores/indicatorstore';
import chartTheme from '../utils/highcharts_theme';
import gimmeNum from '../utils/gimmenum';
import isEqual from '../utils/isequal';
import Indicators from '../utils/indicators';

const CHART_PERIODS = {
	Tick: 1000,
	S30: 30000,
	M1: 60000,
	M2: 120000,
	M4: 240000,
	M5: 300000,
	M8: 480000,
	M12: 720000,
	M15: 900000,
	M30: 1800000,
	H1: 3600000,
	H2: 7200000,
	H4: 14400000,
	H8: 28800000,
	H12: 43200000,
	D1: 86400000
};

const CHART_DATAGROUPS = {
	S30: [
		['second', [30]]
	],
	M1: [
		['minute', [1]]
	],
	M2: [
		['minute', [2]]
	],
	M4: [
		['minute', [4]]
	],
	M5: [
		['minute', [5]]
	],
	M8: [
		['minute', [8]]
	],
	M12: [
		['minute', [12]]
	],
	M15: [
		['minute', [15]]
	],
	M30: [
		['minute', [30]]
	],
	H1: [
		['hour', [1]]
	],
	H2: [
		['hour', [2]]
	],
	H4: [
		['hour', [4]]
	],
	H8: [
		['hour', [8]]
	],
	H12: [
		['hour', [12]]
	],
	D1: [
		['day', [1]]
	]
};

const TICK_CANDLES_WICKS = [
	[.4, .4],
	[.6, .3],
	[.2, .6],
	[.6, .4],
	[.4, .5],
	[.4, .4],
	[.3, .7],
	[.6, .3],
	[.4, .5],
	[.6, .3],
	[.5, .4],
	[.3, .6],
	[.7, .4],
	[.3, .7],
	[.6, .3],
	[.5, .4],
	[.5, .3],
	[.2, .6],
	[.6, .3],
	[.3, .6],
	[.7, .2],
	[.2, .7],
	[.7, .3],
	[.3, .6],
	[.5, .3],
	[.2, .7],
	[.6, .4],
	[.3, .8],
	[.8, .2],
	[.8, .3],
	[.2, .7],
	[.8, .2],
	[.4, .5],
	[.7, .2],
	[.2, .8],
	[.7, .3],
	[.2, .8]
];

const PLOT_LINE_CONFIG = chartTheme.yAxis.plotLines[0];

const PLOT_LINE_LBL_BGS = {
	blue: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAXVBMVEUdcKX///8dcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKVBw+PAAAAAHnRSTlMAAAQIEBQcSU9faHKEiouUqrC/y+Tm6e3x9PX2+PrQEEt8AAAAX0lEQVQoz53NRw6AMAADwZjeewvF/38m4ghCgnjvozXmCo8yfmVeXNjNisutpbvzm410d+m8UHD1Qbq7qJv411Hr9kv6UXLwym1VHBC0VnJAsWsO6ao5xOMgOXjVpzsBZZdEhOe9jLYAAAAASUVORK5CYII=',
	red: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAXVBMVEX+AAD/AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAAHBwesAAAAHnRSTlMAAAQIEBQcSU9faHKEiouUqrC/y+Tm6e3x9PX2+PrQEEt8AAAAX0lEQVQoz53NRw6AMAADwZjeewvF/38m4ghCgnjvozXmCo8yfmVeXNjNisutpbvzm410d+m8UHD1Qbq7qJv411Hr9kv6UXLwym1VHBC0VnJAsWsO6ao5xOMgOXjVpzsBZZdEhOe9jLYAAAAASUVORK5CYII=',
	green: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAWlBMVEUArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQDPXvnSAAAAHXRSTlMABAgQFBxJT19ocoSKi5SqsL/L5Obp7fH09fb4+lCsWawAAABcSURBVCjPnc1HDoAwAANB00Mn1CTg/3+TMxISxHsfLfBWza/eVG53xTXOMd6lYyDjndkPCm64yHhX2I1/HbUev2peJYekC15xQDY5yQHtqTkYrzmU6yI5JP2nuwGVdkGREsGMkQAAAABJRU5ErkJggg==',
	white: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAYAAABA8leGAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC41ZYUyZQAAALNJREFUSEvd1S0KQlEURWGDwWgwOiGTQ7CaHYNJq6MwOQMH4DREMBgEFbEc1wGDyAbvD3IPLvj6hnc5r2NmvzBF89SwGgNscULz1MBSM1xeQqRG5uphjRtCpcbmmMCf4BnhUoNTrfBA2NTob4bwo3FE6Hzs3/b5VVKMscMBoVPjU3SxgF/IK0KmhufoY4Mw/7b31OASc9wRKjW0lP/zQj1RNbLGCH5o9mieGljLj80SjTN7Ao0j5iSCV7WeAAAAAElFTkSuQmCC'
};

const INDICATOR_AXIS_OPTIONS = { // indicators pane
	id: 'indicatorAxis',
	gridLineWidth: 1,
	gridLineColor: '#333',

	title: {
		align: 'high',
		offset: 25,
		rotation: 0,
		y: -5,
		style: {
			color: '#fff'
		},
	},
	labels: {
		//x: 0,
		//y: 0,
		style: {
			color: '#999',
			fontSize: '8px',
			fontWeight: 'bold'
		}
	},
	minorGridLineColor: '#333',
	minorGridLineWidth: 1,
	tickColor: '#17161A',
	tickWidth: 1,
	minorTickInterval: 'auto',
	opposite: true
}

Highcharts.setOptions(JSON.parse(JSON.stringify(chartTheme)));
Highcharts.setOptions({global: {useUTC: false}});

export default class Chart extends React.Component {
	static displayName = 'Chart';

	static propTypes = {
		activeSymbol: React.PropTypes.string,
		askdif: React.PropTypes.number,
		chartpntr: React.PropTypes.string,
		chartprice: React.PropTypes.string,
		chartres: React.PropTypes.string,
		charttype: React.PropTypes.string,
		shift: React.PropTypes.number
	};

	constructor(props) {
		super(props);
		// no "State" in this comp to avoid complex rerendering
		this.Chart = null;
		this.newData = {};
		this.chartData = [];
		this.selectedOrderId = null;
		this.selectedOrderOpenPrice = null;
		this.earlyExitPayout = 0;
		this.prevUpdateTime = 0;
		this.prevPrice = 0;
		this.reloadInt = 0;
		this.chartDataLoaded = false;
		//this.indicatorSettings = {};
		this.chartIndicators = [];
		this.prevlpsig = 0;
		this.errs = 0;
	}

	componentDidMount() {
		Store.listen(this.onData);
		SelectedTradeStore.listen(this.updateTrade);
		IndicatorStore.listen(this.setIndicator);
		this.renderChart();
	}

	componentWillUnmount() {
		Store.unlisten(this.onData);
		SelectedTradeStore.unlisten(this.updateTrade);
		IndicatorStore.unlisten(this.setIndicator);
	}

	componentDidUpdate(prevProps) {
		if (isEqual(prevProps, this.props)) return;
		/*if(prevProps.zoomlvl !== this.props.zoomlvl) {
		 this.Chart.options.plotOptions.candlestick.pointWidth = 3;
		 this.Chart.render();
		 }
		 console.log(this.Chart.options.plotOptions.candlestick.pointWidth)

		 if (prevProps.activeSymbol !== this.props.activeSymbol || prevProps.chartres !== this.props.chartres || prevProps.charttype !== this.props.charttype || prevProps.chartprice !== this.props.chartprice || prevProps.chartpntr !== this.props.chartpntr) this.renderChart();*/
		this.renderChart();
	}

	render() {
		return <div ref="chartNode" className="chartArea"></div>
	}

	onData = () => {
		if (!this.chartDataLoaded) {
			this.chartData = Store.getState().chartData;
			this.populateChart();
			return;
		}
		this.newData = Store.getState().newData;
		this.updateChart();
	};

	populateChart = () => {
// load once history data
		if (!this.chartData.length) return;
		//this.Chart.series[0].setData([]);
		let lastStockDataPoint = this.chartData[this.chartData.length - 1];
		try {
			var lpsig = lastStockDataPoint.reduce((pv, cv) => {
				return pv + cv;
			}, 0);
		} catch (e) {
			return;
		}
		if (!lpsig || (lpsig === this.prevlpsig)) {
			this.errs++;
			if (this.errs === 10) {
				this.renderChart();
			}
			return;
		}
		let chartIsSpline = (this.props.charttype === 'areaspline'),
			chartPeriods = CHART_PERIODS[this.props.chartres],
			askdif = (this.props.chartprice.toLowerCase() === 'ask') ? this.props.askdif : 0,
			xCrop = Math.ceil(lastStockDataPoint[0] / chartPeriods) * chartPeriods;
		if (this.props.chartres !== 'Tick') {
			let data = (askdif === 0) ? this.chartData : this.chartData.map(i => {
				return [
					i[0],
					i[1] + askdif,
					i[2] + askdif,
					i[3] + askdif,
					i[4] + askdif
				];
			});
			this.Chart.series[0].setData(data);
		} else {
			if (chartIsSpline) {
				let splineData = this.chartData.map(i => {
					return [i[0], i[4] + askdif];
				});
				this.Chart.series[0].setData(splineData);
			} else {
				let tickData = this.chartData.map(i => {
					return {
						x: i[0],
						open: i[1] + askdif,
						high: i[2] + askdif,
						low: i[3] + askdif,
						close: i[4] + askdif,
						r: i[5]
					};
				});
				this.Chart.series[0].setData(tickData);
			}
		}
		this.prevPrice = lastStockDataPoint[4];
		if (!chartIsSpline) this.addIndicator();
		this.Chart.xAxis[0].setExtremes(xCrop - chartPeriods * this.props.zoomlvl, xCrop, true);
		this.prevlpsig = lpsig;
		this.errs = 0;
		this.chartDataLoaded = true;
	};

	updateChart = () => {
		// enable live chart updates
		if (!this.chartDataLoaded) return;
		let x = this.newData.x;
		if (!x || x === this.prevUpdateTime) return;
		let chartIsSpline = (this.props.charttype === 'areaspline'),
			chartIsTick = (this.props.chartres === 'Tick'),
			y = (this.props.chartprice.toLowerCase() === 'ask') ? this.newData.y + this.props.askdif : this.newData.y,
			chartPeriods = CHART_PERIODS[this.props.chartres],
			xCrop = Math.ceil(x / chartPeriods) * chartPeriods,
			r = this.newData.r || 0,
			wicks = TICK_CANDLES_WICKS[r],
			topWick = wicks[0],
			bottomWick = wicks[1],
			wickCoeff = 10 * Math.pow(0.1, this.props.shift),
			openPrice = this.prevPrice,
			newPoint = {
				x: x,
				open: openPrice,
				high: Math.max(y, openPrice + topWick * wickCoeff),
				low: Math.min(y, openPrice - bottomWick * wickCoeff),
				close: y,
				r: r
			},
			shiftChart = chartIsTick ? true : ((1000 * Math.ceil(x / 1000)) % chartPeriods === 0),
			indicatorIsOn = !!this.Chart.series[1];
		if (chartIsSpline) {
			this.Chart.series[0].addPoint({x, y, r}, false, shiftChart);
		} else {
			this.Chart.series[0].addPoint(newPoint, false, shiftChart, false);
			if (shiftChart && !chartIsTick) {
				console.log(new Date(newPoint.x).toLocaleTimeString(), 'o:', newPoint.open, 'h:', newPoint.high, 'l:', newPoint.low, 'c:', newPoint.close);
				// if indicator is on, redraw indicator
				if (indicatorIsOn) {
					window.setTimeout(() => {
						this.addIndicator();
					}, 2000);
				}
			}
			// if indicator is on, update indicator
			if (indicatorIsOn && chartIsTick) this.updateIndicator();
		}
		if (this.selectedOrderId === null) this.redrawPlotline();
		this.Chart.xAxis[0].setExtremes(xCrop - chartPeriods * this.props.zoomlvl, xCrop, true);
		this.prevUpdateTime = x;
		this.prevPrice = y;
	};

	redrawPlotline = () => {
		this.Chart.yAxis[0].removePlotLine('pl1');
		let y = (this.props.chartprice.toLowerCase() === 'ask') ? this.newData.y + this.props.askdif : this.newData.y;
		if (!y) return;
		let plotLineOptions = PLOT_LINE_CONFIG,
			earlyExitPayout = this.earlyExitPayout,
			plotLineLblBg;
		plotLineOptions.id = 'pl1';
		plotLineOptions.value = y;
		plotLineOptions.label.text = y.toFixed(this.props.shift);
		switch (true) {
			case !earlyExitPayout:
				plotLineOptions.color = '#1D70A5';
				plotLineLblBg = PLOT_LINE_LBL_BGS.blue;
				break;
			case (earlyExitPayout > 0):
				plotLineOptions.color = '#00ad00';
				plotLineLblBg = PLOT_LINE_LBL_BGS.green;
				break;
			case (earlyExitPayout < 0):
				plotLineOptions.color = 'red';
				plotLineLblBg = PLOT_LINE_LBL_BGS.red;
				break;
			default:
				plotLineOptions.color = '#1D70A5';
				plotLineLblBg = PLOT_LINE_LBL_BGS.blue;
		}
		plotLineOptions.label.style.background = 'url(data:image/png;base64,' + plotLineLblBg + ') no-repeat -2px -1px';
		this.Chart.yAxis[0].addPlotLine(plotLineOptions);
	};

	redrawOrderPlotline = () => {
		this.Chart.yAxis[0].removePlotLine('pl2');
		if (!this.selectedOrderId) return;
		var plotLineOptions = JSON.parse(JSON.stringify(PLOT_LINE_CONFIG)),
			plotLineValue = this.selectedOrderOpenPrice;
		plotLineOptions.id = 'pl2';
		plotLineOptions.color = '#fff';
		plotLineOptions.value = plotLineValue;
		plotLineOptions.label.text = plotLineValue.toFixed(this.props.shift);
		plotLineOptions.label.style.color = '#222';
		plotLineOptions.label.style.background = 'url(data:image/png;base64,' + PLOT_LINE_LBL_BGS.white + ') no-repeat -2px -1px';
		this.Chart.yAxis[0].addPlotLine(plotLineOptions);
	};

	setIndicator = () => {
		/*var nextIndicators = IndicatorStore.getState().channels[this.props.activeSymbol].chartIndicators,
		 thisIndicators = this.chartIndicators;
		 console.log(nextIndicators, thisIndicators)
		 this.chartIndicators = nextIndicators;

		 return;
		 if (isEqual(nextIndicators[0], thisIndicators[0])) return;
		 this.chartIndicators = nextIndicators;*/
		this.chartIndicators = IndicatorStore.getState().channels[this.props.activeSymbol].chartIndicators;
		this.addIndicator();
	};

	addIndicator = () => {
		this.clearIndicator();
		let chartIndicators = this.chartIndicators;
		if (!chartIndicators.length) return;
		for (var i = 0, z = chartIndicators.length; i < z; i++) {
			let currIndicator = chartIndicators[i],
				indicatorAxis = currIndicator.separate ? 1 : 0,
			// newlinecolor = (i === 0) ? null : '#ff0000',
				indicatorSeries = Indicators.getIndicatorData(currIndicator, this.Chart.series[0]);
			if (indicatorAxis) {
				let chartNodeHeight = React.findDOMNode(this.refs.chartNode).clientHeight,
					indicatorAxisOptions = JSON.parse(JSON.stringify(INDICATOR_AXIS_OPTIONS));
				indicatorAxisOptions.top = chartNodeHeight * 0.75;
				indicatorAxisOptions.height = chartNodeHeight * 0.25 - 25;
				indicatorAxisOptions.title.text = currIndicator.indicator;
				indicatorAxisOptions.title.x = this.Chart.yAxis[0].width * -1;
				this.Chart.yAxis[0].update({
					height: chartNodeHeight * 0.7,
				});
				this.Chart.addAxis(indicatorAxisOptions);
			}
			for (let j = 0, k = indicatorSeries.length; j < k; j++) {
				let currSeries = indicatorSeries[j];
				this.Chart.addSeries({
					id: currSeries.id,
					name: currSeries.name,
					type: currSeries.type,
					color: currSeries.color,
					lineWidth: currSeries.lineWidth,
					dashStyle: currSeries.dashStyle,
					data: currSeries.data,
					yAxis: indicatorAxis
				});
			}
		}
	};

	clearIndicator = () => {
		if (this.Chart.series.length === 1) return;
		while (this.Chart.series.length > 1) this.Chart.series[1].remove(true);
		if (this.Chart.yAxis.length > 1) {
			while (this.Chart.yAxis.length > 1) this.Chart.yAxis[1].remove(true);
			//this.Chart.get('indicatorAxis').remove();
			let mainAxisOptions = this.Chart.yAxis[0].options;
			mainAxisOptions.height = React.findDOMNode(this.refs.chartNode).clientHeight - 25;
			this.Chart.yAxis[0].update(mainAxisOptions);
		}
	};

	//noinspection FunctionWithMultipleLoopsJS
	updateIndicator = () => {
		let chartIndicators = this.chartIndicators;
		if (!chartIndicators.length) return;
		for (let i = 0, z = chartIndicators.length; i < z; i++) {
			let currIndicator = chartIndicators[i],
				indicatorData = Indicators.getIndicatorData(currIndicator, this.Chart.series[0]);
			for (let j = 0, k = indicatorData.length; j < k; j++) {
				let currData = indicatorData[j].data;
				this.Chart.get(indicatorData[j].id).addPoint(currData[currData.length - 1], false, true, false);
			}
		}
	};

	updateTrade = () => {
		let selectedTradeIndex = SelectedTradeStore.getState().selectedTradeIndex;
		if (selectedTradeIndex !== null && SelectedTradeStore.getState().openTrades[selectedTradeIndex].symbol === this.props.activeSymbol) {
			let selectedOrder = SelectedTradeStore.getState().openTrades[selectedTradeIndex];
			this.selectedOrderId = selectedOrder.ticket;
			this.selectedOrderOpenPrice = Number(selectedOrder.open);
			this.earlyExitPayout = gimmeNum(selectedOrder['profit/loss']);
			this.redrawOrderPlotline();
			this.redrawPlotline();
			this.reset = true;
			return;
		}

		if (this.reset) {
			this.selectedOrderId = null;
			this.selectedOrderOpenPrice = null;
			this.earlyExitPayout = 0;
			this.redrawOrderPlotline();
			this.reset = false;
		}
	};

	renderChart = () => {
		this.chartDataLoaded = false;
		let chartres = this.props.chartres,
			charttype = this.props.charttype,
			chartTypeToRender = (charttype === 'areaspline' ? 'Chart' : 'StockChart'),
			dataGrouping = CHART_DATAGROUPS[chartres] ? {
				enabled: true,
				forced: true,
				units: CHART_DATAGROUPS[chartres]
			} : {enabled: false},
			shift = this.props.shift,
			chartNode = React.findDOMNode(this.refs.chartNode);
		//noinspection Eslint
		this.Chart = new Highcharts[chartTypeToRender]({
			tooltip: {
				crosshairs: (this.props.chartpntr === 'measure') ? [true, true] : [false, false],
				valueDecimals: shift,
				positioner: function () {
					return {x: 0, y: 0};
				},
				pointFormatter: function () {
					if (this.series.index === 0) {
						let pointDiff = (this.open - this.close),
							result = typeof this.r === 'undefined' ? '' : this.r + ', ';
						if (isNaN(pointDiff)) {
							return '<span style="color:white; margin: auto 2px"> \u25B6 </span> ' + result + this.y;
						}
						let delimColor = (pointDiff < 0) ? 'green' : 'red';
						let distance = Math.round((this.close - this.open) / Math.pow(0.1, shift));
						if (pointDiff === 0) delimColor = 'white';
						return '<span style="color:' + delimColor + '; margin: auto 2px"> \u25B6 </span>' + result + ' <b>O</b> ' + this.open.toFixed(shift) + ' | <b>H</b> ' + this.high.toFixed(shift) + ' | <b>L</b> ' + this.low.toFixed(shift) + ' | <b>C</b> ' + this.close.toFixed(shift) + ' | <b>D</b> ' + (distance > 0 ? '+' : '') + distance;
					} else {
						return '<br>' + this.series.name + '<span style="color:' + this.series.color + '; margin: auto 2px"> \u25B6 </span>' + this.y;
					}
				},
				backgroundColor: 'transparent'
			},
			yAxis: {
				id: 'mainAxis',
				height: chartNode.clientHeight - 25,
				gridLineWidth: 0,
				gridLineColor: 'transparent',
				labels: {
					x: (charttype === 'areaspline') ? 5 : 35
				}
			},
			plotOptions: {
				candlestick: {
					pointWidth: Math.round(720 / this.props.zoomlvl)
				}
			},
			rangeSelector: {enabled: false},
			navigator: {enabled: false},
			scrollbar: {enabled: false},
			chart: {
				renderTo: chartNode,
				type: charttype
			},
			series: [
				{
					dataGrouping: dataGrouping,
					name: this.props.activeSymbol,
					type: charttype,
					data: []
				}
			]
		}, () => {
			this.prevUpdateTime = 0;
		});
		Actions.getChart(this.props.activeSymbol, chartres);
		if (this.reloadInt) clearInterval(this.reloadInt);
		this.reloadInt = setInterval(()=> {
			this.renderChart();
		}, 1800000);
	};

}
