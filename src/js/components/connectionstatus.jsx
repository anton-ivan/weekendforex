'use strict';

export default class ConnectionStatus extends React.Component {
	static displayName = 'ConnectionStatus';

	static propTypes = {
		online: React.PropTypes.bool.isRequired
	};

	static defaultProps = {
		online: true
	};

	shouldComponentUpdate(nextProps) {
		return (nextProps.online !== this.props.online);
	}

	render() {
		let status = this.props.online ? 'online' : 'offline',
			statusColor = this.props.online ? '#5ECD24' : 'red';
		return (
			<div>
				Connection Status:
				<span className={this.props.online ? 'led active' : 'led alert'}></span>
				<span className="connstatus" style={{color: statusColor}}> {status}</span>
			</div>
		);
	}
}
