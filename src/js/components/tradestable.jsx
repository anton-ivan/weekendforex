'use strict';

import Store from '../stores/tradetablestore';
import Actions from '../actions/actions';
import Griddle from 'griddle-react';
import TradeTableButtons from './tradetablebuttons.jsx';
import DateDisplay from './datedisplay.jsx';

const columnMetaOpenTrades = [
		{columnName: 'profit/loss', customComponent: TradeTableButtons},
		{columnName: 'open time', customComponent: DateDisplay}
	],
	columnMetaClosedTrades = [
		{columnName: 'open time', customComponent: DateDisplay},
		{columnName: 'close time', customComponent: DateDisplay}
	];

export default class TradesTable extends React.Component {
	static displayName = 'TradesTable';

	static propTypes = {
		thsclose: React.PropTypes.array,
		thsopen: React.PropTypes.array.isRequired,
		viewButtons: React.PropTypes.array.isRequired
	};

	static defaultProps = {
		viewButtons: [
			{label: 'open games', value: 'opentrades'},
			{label: 'closed games', value: 'closetrades'}
		],
		thsopen: ['ticket', 'symbol', 'bet amount', 'payout', 'roi', 'type', 'open', 'expiry', 'open time', 'time', 'profit/loss'],
		thsclose: ['ticket', 'symbol', 'bet amount', 'payout', 'roi', 'type', 'open', 'close', 'expiry', 'open time', 'close time', 'EE', 'profit/loss']
	};

	constructor(props) {
		super(props);
		this.state = Store.getState();
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		let noDataMsg = 'There are no open trades on this chart!',
			viewSelectorContent = this.props.viewButtons.map((m, i)=> {
				//noinspection JSPotentiallyInvalidUsageOfThis
				return (
					<li className={m.value === this.state.activeView ? 'active' : ''} data-value={m.value} key={'tableview' + i} onClick={this.changeView}>{m.label}</li>
				);
			});
		return (
			<div className="panel tablepanel">
				<ul className="tabbar">
				{viewSelectorContent}
				</ul>
				<div>
					{(this.state.activeView === 'opentrades') ?
						<Griddle key="opentrades" results={this.state.openTrades} columns={this.props.thsopen} useGriddleStyles={false} resultsPerPage={5} columnMetadata={columnMetaOpenTrades} noDataMessage={noDataMsg} initialSort="open time" initialSortAscending={false} tableClassName="openTradesTable"/> :
						<Griddle key="closetrades" results={this.state.closeTrades} columns={this.props.thsclose} useGriddleStyles={false} resultsPerPage={5} columnMetadata={columnMetaClosedTrades} noDataMessage={noDataMsg} initialSort="close time" initialSortAscending={false}/>}
				</div>
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};

	changeView = (e) => {
		Actions.setTradeTableView({value: e.target.getAttribute('data-value')});
	};

}
