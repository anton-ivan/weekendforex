'use strict';

export default class DateDisplay extends React.Component {
	static displayName = 'DateDisplay';
	static propTypes = {
		data: React.PropTypes.number.isRequired
	};

	static defaultProps = {data: 0};

	render() {
		return <span>{new Date(this.props.data).toLocaleString()}</span>;
	}
}
