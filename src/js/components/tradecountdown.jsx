'use strict';

export default class TradeCountdown extends React.Component {
	static displayName = 'TradeCountdown';

	static propTypes = {
		time: React.PropTypes.number.isRequired
	};

	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps) {
		return (nextProps.time !== this.props.time);
	}

	render() {
		var timeleft = this.props.tradeResult ? 0 : this.props.time,
			minsleft = timeleft === 0 ? '--' : Math.floor(timeleft / 60),
			secsleft = timeleft === 0 ? '--' : timeleft - minsleft * 60;
		minsleft = (minsleft < 10) ? '0' + minsleft : '' + minsleft;
		secsleft = (secsleft < 10) ? '0' + secsleft : '' + secsleft;
		return (
			<div className="timerem">
				<p>Time Remaining</p>
				<time>{minsleft}:{secsleft}</time>
			</div>
		);
	}
}
