'use strict';

import Actions from '../actions/actions';

export default class ExpirySlider extends React.Component {
	static displayName = 'ExpirySlider';

	static propTypes = {
		active: React.PropTypes.bool.isRequired,
		symbol: React.PropTypes.string.isRequired,
		ticks: React.PropTypes.array.isRequired,
		value: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string])
	};

	static defaultProps = {
		ticks: ['15', '30', '60', '120', '180', '300', '600', '900']
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
	}

	componentDidMount() {
		this.positionSliderThumb();
	}

	shouldComponentUpdate(nextProps) {
		return (nextProps.value !== this.props.value || nextProps.active !== this.props.active);
	}

	componentDidUpdate(){
		this.positionSliderThumb();
	}

	render() {
		let sliderContent = this.props.ticks.map((m, i)=> {
			return (
				<span className={this.props.value == m ? 'active' : ''} data-value={m} key={i} onClick={this.setValue}>
                {m + '\nsec'}
                </span>
			);
		});
		return (<div className="sliderdiv">
				<label>Expiry:</label>

				<div className="sliderpath" ref="sliderpath">
					{sliderContent}
					<div className="sliderthumb" ref="sliderthumb">
					</div>
				</div>
			</div>
		);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	positionSliderThumb = () => {
		let val = this.props.value || this.props.ticks[0],
			sliderPath = React.findDOMNode(this.refs.sliderpath),
			sliderThumb = React.findDOMNode(this.refs.sliderthumb),
			currentTick = sliderPath.querySelector('span[data-value="' + val + '"]'),
			sliderThumbPosition = Math.round((currentTick.getBoundingClientRect().left - sliderPath.getBoundingClientRect().left) - 5);
		sliderThumb.style.left = sliderThumbPosition + 'px';
	};

	setValue = (e) => {
		if(!this.props.active) return;
		Actions.setExpiryValue(this.addSymbol({value: e.currentTarget.getAttribute('data-value')}));
	};
}
