'use strict';
import Store from '../stores/tickerstore';

export default class Ticker extends React.Component {

	static displayName = 'Ticker';

	static propTypes = {
		activeSymbol: React.PropTypes.string
	};

	constructor(props) {
		super(props);
		this.state = {
			tickerQueue: []
		};
	}

	componentDidMount() {
		Store.listen(this.onData);
	}

	componentWillUnmount() {
		Store.unlisten(this.onData);
	}

	render() {
		if (!this.props.activeSymbol) return null;
		let tickerContent = this.state.tickerQueue.map((m, i)=> {
			let valueClass = (m > 0) ? 'down' : 'up';
			if (!m) valueClass = '';
			return (
				<div className={valueClass} key={i}>{Math.abs(m)}</div>
			);
		});

		return (
			<div className="ticker">
				<div className="tickerscroller">
					{tickerContent}
				</div>
			</div>
		);
	}

	onData = () => {
		let symbol = this.props.activeSymbol;
		if (!symbol) return;
		this.setState({tickerQueue: Store.getState().channels[symbol].tickerQueue});
	};

}
