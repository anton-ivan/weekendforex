'use strict';

import Actions from '../actions/actions';
import Store from '../stores/tickerstore';
import InitConfig from '../utils/initconfig';
import ExpirySlider from './expiryslider.jsx';
import classNames from '../utils/classnames';

export default class TradeControlPanel extends React.Component {
	static displayName = 'TradeControlPanel';

	static propTypes = {
		active: React.PropTypes.bool.isRequired,
		alias: React.PropTypes.string,
		amount: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
		balance: React.PropTypes.number,
		expiry: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
		symbol: React.PropTypes.string.isRequired
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.waitasec = false;
		this.tickSign = 0;
	}

	componentDidMount() {
		Store.listen(this.onTick);
	}

	componentWillUnmount() {
		Store.unlisten(this.onTick);
	}

	shouldComponentUpdate(nextProps) {
		return (this.props.active || nextProps.active !== this.props.active);
	}

	render() {
		let assetLabelArr = this.props.alias.split('/'),
		// this es2015 shit is too slow yet
		// assetInfoContent = `<span class="down">${assetLabelArr[0]}</span> / <span class="up">${assetLabelArr[1]}</span>`,
			payout = Intl.NumberFormat('en-GB', {
				minimumFractionDigits: 2
			}).format(Math.round((this.props.amount + (this.props.amount * InitConfig.ROI / 100)) + 'e+2') + 'e-2'),
			insuffBalance = this.props.amount > this.props.balance,
			downBtnClass = classNames(
				'bigbtn down',
				{active: this.tickSign < 0},
				{disabled: (insuffBalance)}
			),
			upBtnClass = classNames(
				'bigbtn up',
				{active: this.tickSign > 0},
				{disabled: (insuffBalance)}
			);
		return (
			<div className={this.props.active ? 'ctrlpnl active' : 'ctrlpnl'}>
				<div className="ctrlpnlHead">
					<div className="symLbl">
					<span className={this.props.active ? 'down' : ''}>
						{assetLabelArr[0]}
						</span>
						<span className="vsepar"></span>
						<span className={this.props.active ? 'up' : ''}>
						{assetLabelArr[1]}
						</span>
					</div>
					<div>
						<span className="whitetxt">payout:</span> <span className="payout">{InitConfig.ROI}%</span>
					</div>
				</div>
				<div className="ctrlpnlBody">
					<ExpirySlider symbol={this.props.symbol} value={this.props.expiry} active={this.props.active}/>

					<div className="relpos flexrow">
						<div className={downBtnClass} data-value={InitConfig.PUT_FLAG} onClick={this.createTrade}>
							trend down
							<div className="trendicon"></div>
							<div className="fat">put</div>
							<div className="info">potential payout:</div>
							<div className="payout">&pound;{payout}</div>
						</div>
						<div className={upBtnClass} data-value={InitConfig.CALL_FLAG} onClick={this.createTrade}>
							trend up
							<div className="trendicon"></div>
							<div className="fat">call</div>
							<div className="info">potential payout:</div>
							<div className="payout">&pound;{payout}</div>
						</div>
						<div className="amountdiv">
							<select onChange={this.handleChange} disabled={!this.props.active} defaultValue="100">
								<option value="5">&pound;5</option>
								<option value="10">&pound;10</option>
								<option value="25">&pound;25</option>
								<option value="50">&pound;50</option>
								<option value="100">&pound;100</option>
								<option value="250">&pound;250</option>
								<option value="500">&pound;500</option>
								<option value="1000">&pound;1000</option>
								<option value="2500">&pound;2500</option>
								<option value="other">other</option>
							</select>
							<input className="amount" type="number" disabled={!this.props.active} min={InitConfig.MIN_BET} max={InitConfig.MAX_BET} ref="amountInput" onChange={this.handleChange}/>
						</div>
					</div>
				</div>
			</div>
		);
	}

	onTick = () => {
		if (!this.props.active) return false;
		let newTickSign = Store.getState().channels[this.props.symbol].tickSign;
		if (newTickSign === this.tickSign) return false;
		this.tickSign = newTickSign;
		this.forceUpdate();
	};

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	setActiveSymbol = () => {
		if (this.props.active) return;
		Actions.setActiveSymbol(this.props.symbol);
	};

	handleChange = (e) => {
		let control = e.currentTarget,
			val = control.value,
			amountInput = React.findDOMNode(this.refs.amountInput);
		if (control.tagName.toLowerCase() === 'select') {
			amountInput.value = '';
			amountInput.style.display = (!/^\d{0,4}$/.test(val)) ? 'inline-block' : 'none';
		}

		if (!/^\d{0,4}$/.test(val) || parseInt(val) < InitConfig.MIN_BET || parseInt(val) > InitConfig.MAX_BET) {
			val = 0;
		}
		Actions.setAmountValue(this.addSymbol({value: val}));
	};

	createTrade = (e) => {
		if (!this.props.active || this.props.amount > this.props.balance || this.waitasec) return;
		Actions.createTrade(this.addSymbol({
			flags: parseInt(e.currentTarget.getAttribute('data-value')),
			expiration_in: parseInt(this.props.expiry),
			amount: parseFloat(this.props.amount)
		}));
		this.waitasec = true;
		window.setTimeout(() => {
			this.waitasec = false;
		}, 1000);
	};
}
