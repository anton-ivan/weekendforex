'use strict';

import Actions from '../actions/actions';
import getStylesheetRule from '../utils/getstylesheetrule';
import classNames from '../utils/classnames';

// CIRCLE_DASH must match --clockfaceSvgCircleStrokeDasharray var in the element-metrics.css
const CIRCLE_DASH = parseInt(getStylesheetRule('.clockface svg circle:first-of-type').strokeDasharray) || 472;
// SVG_RADIUS must match half of the --bigrndbtn-first-div-size-size var in the element-metrics.css
const SVG_RADIUS = parseInt(getStylesheetRule('.clockface').width) / 2 || 90;

export default class BigRoundButton extends React.Component {
	static displayName = 'BigRoundButton';

	static propTypes = {
		expiry: React.PropTypes.number,
		earlyExitPayout: React.PropTypes.number,
		selectedTradeId: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
		time: React.PropTypes.number
	};

	static defaultProps = {
		expiry: 10,
		time: 10
	};

	render() {
		let tradeResult = this.props.tradeResult || null,
			eePayout = this.props.earlyExitPayout || null,
			callText = 'PLACE YOUR TRADE',
			cirleTime = tradeResult ? 0 : Math.round(this.props.time * (CIRCLE_DASH / this.props.expiry)),
			circleColorFac = tradeResult ? tradeResult : eePayout,
			circleColor = (circleColorFac < 0) ? 'url(#redGrad)' : 'url(#greenGrad)',
			bigButtonClass = classNames({
				active: this.props.selectedTradeId && !tradeResult,
				loss: (!tradeResult && eePayout < 0 || tradeResult && tradeResult < 0)
			}),
			fiveSecsLeft = (this.props.time && this.props.time <= 5),
			eePayoutInt = (eePayout > 0) ? Math.floor(eePayout) : Math.ceil(eePayout),
			eePayoutDec = Math.abs(eePayout - eePayoutInt).toFixed(2).replace(/^[0]+/g, ''),
			tradeResultInt = (tradeResult > 0) ? Math.floor(tradeResult) : Math.ceil(tradeResult),
			tradeResultDec = Math.abs(tradeResult - tradeResultInt).toFixed(2).replace(/^[0]+/g, '');

		switch (true) {
			case (tradeResult !== null):
				callText = '<span class="nobold">' + ((tradeResult > 0) ? 'YOU WON' : 'TRADE CLOSED') + '</span><div class="eeValDiv"><span class="eeValSpan">' + ((tradeResult < 0) ? '-' : '') + '&pound;</span><span class="eeValSpan">' + Math.abs(tradeResultInt) + '</span><span class="eeValSpan">' + tradeResultDec + '</span></div>';
				break;
			case !this.props.selectedTradeId:
				callText = 'PLACE YOUR TRADE';
				break;
			case (!!eePayout):
				callText = '<span class="nobold">early exit:</span><div class="eeValDiv"><span class="eeValSpan">' + ((eePayout < 0) ? '-' : '') + '&pound;</span><span class="eeValSpan">' + Math.abs(eePayoutInt) + '</span><span class="eeValSpan">' + eePayoutDec + '</span></div>';
				break;
		}
		return (
			<div className="bigRndBtn">
				<div>
					<div className="clockface">
						<svg height={SVG_RADIUS * 2} width={SVG_RADIUS * 2}>
							<defs>
								<linearGradient id="redGrad" x1="1" x2="0" y1="0" y2="0">
									<stop offset="0%" className="redGrad1"/>
									<stop offset="69%" className="redGrad2"/>
									<stop offset="100%" className="redGrad3"/>
								</linearGradient>
								<linearGradient id="greenGrad" x1="1" x2="0" y1="0" y2="0">
									<stop offset="0%" className="greenGrad1"/>
									<stop offset="100%" className="greenGrad2"/>
								</linearGradient>
							</defs>
							<circle cx={SVG_RADIUS} cy={SVG_RADIUS} r={(SVG_RADIUS)-15} stroke={circleColor} style={{strokeDashoffset: cirleTime}}/>
							<circle cx={SVG_RADIUS} cy={SVG_RADIUS} r={(SVG_RADIUS)-15}/>
						</svg>
						<div className={bigButtonClass} onClick={this.requestEarlyExit}>
							<div>
								<h6 className={tradeResult || fiveSecsLeft ? 'blink' : ''} dangerouslySetInnerHTML={{__html: callText}}/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);

	}

	requestEarlyExit = () => {
		if (!this.props.selectedTradeId || this.props.time < 1 || !this.props.earlyExitPayout) return;
		Actions.requestEarlyExit(this.props.selectedTradeId);
	};
}
