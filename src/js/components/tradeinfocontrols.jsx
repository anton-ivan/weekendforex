'use strict';

// higher order component wrapping EE exit button, circular countdown bar and trade countdown
import Store from '../stores/tradetablestore.js';
import BigRoundButton from './bigroundbutton.jsx';
import TradeCountdown from './tradecountdown.jsx';
import gimmeNum from '../utils/gimmenum';


export default class TradeInfoControls extends React.Component {

	static displayName = 'TradeInfoControls';

	static propTypes = {
		symbol: React.PropTypes.string
	};

	constructor(props) {
		super(props);
		this.state = {
			selectedTrade: {},
			tradeResult: null
		};
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	/*componentWillUpdate(nextProps, nextState) {
		console.log(nextState.tradeResult, this.state.tradeResult)
	}*/

	render() {
		let eePayout = this.state.selectedTrade['profit/loss'] ? gimmeNum(this.state.selectedTrade['profit/loss']) : null;
		return (
			<div>
				<BigRoundButton selectedTradeId={this.state.selectedTrade.ticket} earlyExitPayout={eePayout} expiry={this.state.selectedTrade.expiry || 10} time={this.state.selectedTrade.time || 10} tradeResult={this.state.tradeResult || null}/>
				<TradeCountdown time={this.state.selectedTrade.time || 0} tradeResult={this.state.tradeResult || null}/>
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getActiveTrade());
	};
}
