'use strict';

// higher order component wrapping dumb tradecontrolpanels
import Actions from '../actions/actions';
import Store from '../stores/tradecontrolstore';
import TradeControlPanel from './tradecontrolpanel.jsx';

export default class TradeControls extends React.Component {
	static displayName = 'TradeControls';

	static propTypes = {
		activeSymbol: React.PropTypes.string,
		online: React.PropTypes.bool,
		user: React.PropTypes.object
	};

	constructor(props) {
		super(props);
		this.state = Store.getState();
	}

	componentDidMount() {
		this.init = true;
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		let leftColContent = Object.keys(this.state.channels).map((m)=> {
			let chan = this.state.channels[m],
				symbol = chan.symbol;
			return (
				<TradeControlPanel symbol={symbol} alias={chan.alias} expiry={chan.expiry} amount={chan.amount} active={chan.symbol === this.props.activeSymbol && this.props.user.userbalance > 5 && this.props.online} balance={this.props.user.userbalance} key={'ctrl' + symbol}/>
			);
		});
		return (
			<div>
				<div className="modeSwitch">
				<div className="rswitch">
					<input id="FXModeRadio" name="appMode" type="radio" value="FX" onChange={this.onModeChange}/>
					<label htmlFor="FXModeRadio"><span className="icon"></span>FX</label>
					<input id="BXModeRadio" name="appMode" type="radio" value="BX" defaultChecked onChange={this.onModeChange}/>
					<label htmlFor="BXModeRadio">BX<span className="icon"></span></label>
				</div>
					</div>
				{leftColContent}
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};

	onModeChange = (e) => {
		Actions.setTradeMode(e.currentTarget.value);
	}
}
