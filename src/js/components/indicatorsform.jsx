'use strict';

import Actions from '../actions/actions';

export default class IndicatorsForm extends React.Component {
	static displayName = 'IndicatorsForm';

	static propTypes = {
		activeSymbol: React.PropTypes.string
	};

	constructor(props) {
		super(props);
		this.state = {
			indicator: 'MA',
			type: 'simple',
			base: 'close',
			period: '14',
			periodFast: '12',
			periodSlow: '26',
			periodSignal: '9',
			symbol: this.props.activeSymbol
		};
		if (!this.props.activeSymbol) throw new Error('Symbol undefined');
	}

	render() {
		let indicatorIsMA = this.state.indicator === 'MA',
			indicatorIsBB = this.state.indicator === 'BB',
			indicatorIsMACD = this.state.indicator === 'MACD';
		return (
			<form name="indicatorForm" onSubmit={this.addIndicator}>
				<table>
					<tr>
						<td>
							<label>Indicator:</label>
						</td>
						<td>
							<select name="indicator" value={this.state.indicator} onChange={this.onIndicatorChange}>
								<option value="MA">MA</option>
								<option value="BB">BB</option>
								<option value="RSI">RSI</option>
								<option value="ATR">ATR</option>
								<option value="CCI">CCI</option>
								<option value="ADX">ADX</option>
								<option value="MACD">MACD</option>
							</select>

						</td>
						<td>
							<label>
								{indicatorIsMACD ? 'Period Fast:' : 'Period:'}
							</label>
						</td>
						<td>
							<input name={indicatorIsMACD ? 'periodFast' : 'period'} type="number" min="1" max="30" size="2" maxLength="2" pattern="\d{2}" required defaultValue={this.state.periodFast} onChange={this.handleChange}/>
						</td>
					</tr>
					<tr>
						<td>
							{indicatorIsMA || indicatorIsBB ? <label>Base:</label> : null }

							{indicatorIsMACD ? <label>Period Slow:</label> : null }
						</td>
						<td>
							{indicatorIsMA || indicatorIsBB ?
								<select name="base" value={this.state.base} onChange={this.handleChange}>
									<option value="open">Open</option>
									<option value="high">High</option>
									<option value="low">Low</option>
									<option value="close">Close</option>
								</select> : null}

							{indicatorIsMACD ?
								<input name="periodSlow" type="number" min="1" max="30" size="2" maxLength="2" pattern="\d{2}" required defaultValue={this.state.periodSlow} onChange={this.handleChange}/> : null}
						</td>
						<td>
							{indicatorIsMA ? <label>Type:</label> : null }
							{indicatorIsMACD ? <label>Period Signal:</label> : null }
						</td>
						<td>
							{indicatorIsMA ? <select name="type" value={this.state.type} onChange={this.handleChange}>
								<option value="simple">Simple</option>
								<option value="exponential">Exponential</option>
								<option value="weighted">Weighted</option>
								<option value="cumulative">Cumulative</option>
							</select> : null}
							{indicatorIsMACD ?
								<input name="periodSignal" type="number" min="1" max="30" size="2" maxLength="2" pattern="\d{2}" required defaultValue={this.state.periodSignal} onChange={this.handleChange}/> : null}
						</td>
					</tr>
				</table>
				<div className="formbuttons">
					<input type="hidden" name="symbol" value={this.props.activeSymbol}/>
					<button type="submit">Add / View</button>
					<button type="cancel" onClick={this.closeModal}>Clear / Cancel</button>
				</div>
			</form>
		);
	}

	onIndicatorChange = (e) => {
		this.setState({indicator: e.target.value});
	};

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	};

	addIndicator = (e) => {
		e.preventDefault();
		Actions.setIndicator(this.state);
	};

	closeModal = (e) => {
		e.preventDefault();
		Actions.setIndicator({symbol: this.props.activeSymbol});
	};
}
