'use strict';

import Actions from '../actions/actions';
//const CHART_LENGTH = 100;

class ChartStore {
	constructor() {
		this.activeSymbol = null;
		this.state = {
			newData: {},
			chartData: []
		};
		this.bindAction(Actions.initApp, ChartStore.addChannels);
		this.bindAction(Actions.setActiveSymbol, this.setActiveSymbol);
		this.bindAction(Actions.updateTicker, this.updateChart);
		this.bindAction(Actions.getChart, this.getChart);
	}

	static addChannels(appSymbols) {
		if (!appSymbols.length) return false;
		this.activeSymbol = appSymbols[0].symbol;
		return false;
	}

	setActiveSymbol(sym) {
		this.activeSymbol = sym;
	}

	updateChart(quoteData) {
		let symbol = quoteData.symbol;
		if (symbol !== this.activeSymbol) return false;
		let x = quoteData.time,
			y = quoteData.bid,
			r = quoteData.change;
		this.setState({newData: {x, y, r}});
	}

	getChart(symbolchartDataObj) {
		let symbol = symbolchartDataObj.symbol;
		if (symbol !== this.activeSymbol) return false;
		if (!symbolchartDataObj.chartData.length) return false;
		this.setState({chartData: symbolchartDataObj.chartData});
	}
}

export default alt.createStore(ChartStore, 'ChartStore');
