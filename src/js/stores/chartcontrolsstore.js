'use strict';
// todo - try immutable state here
// todo - move here chart controls config stuff from initconfig

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';

const ZOOM_LVLS = [60, 90, 120];


class ChartControlsStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, ChartControlsStore.addChannels);
		this.bindAction(Actions.setChartControl, this.setChartControl);
		this.bindAction(Actions.setChartZoom, this.setChartZoom);
	}

	static addChannels(appSymbols) {
		if (!appSymbols.length) return false;
		appSymbols.map(m => {
			return (this.channels[m.symbol] = {
				symbol: m.symbol,
				alias: m.alias,
				shift: m.shift || 3,
				askdif: m.ask_dif || 0,
				chartprice: InitConfig.INIT_CHART_PRICE,
				chartres: InitConfig.INIT_CHART_RES,
				chartpntr: InitConfig.INIT_CHART_PNTR,
				charttype: InitConfig.INIT_CHART_TYPE,
				zoomlvl: ZOOM_LVLS[ZOOM_LVLS.length-1]
			});
		});
	}

	setChartControl(valSymObj) {
		let symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan || !chan[valSymObj.name]) return false;
		chan[valSymObj.name] = valSymObj.value;
	}

	setChartZoom(valSymObj) {
		let symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		let zoomAction = valSymObj.zoom,
			currZoomLvl = ZOOM_LVLS.indexOf(chan.zoomlvl),
			newZoomLvl = null;
		if(zoomAction === 'in'){
			if (currZoomLvl > 0) newZoomLvl = ZOOM_LVLS[--currZoomLvl];
		} else {
			if (currZoomLvl < ZOOM_LVLS.length - 1) newZoomLvl = ZOOM_LVLS[++currZoomLvl];
		}
		if(!newZoomLvl) return false;
		chan.zoomlvl = newZoomLvl;
	}
}

export default alt.createStore(ChartControlsStore, 'ChartControlsStore');
