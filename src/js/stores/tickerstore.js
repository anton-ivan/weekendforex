'use strict';

import Actions from '../actions/actions';
import getResultSign from '../utils/getresultsign';

const MAX_TICKER_LENGTH = 19;

class TickerStore {
	constructor() {
		this.serverTs = new Date().getTime();
		this.channels = {};
		this.bindAction(Actions.initApp, TickerStore.addChannels);
		this.bindAction(Actions.initTicker, this.initTicker);
		this.bindAction(Actions.updateTicker, this.updateTicker);
	}

	static addChannels(appSymbols) {
		if (!appSymbols.length) return false;
		appSymbols.map(m => {
			return (this.channels[m.symbol] = {
				tickerQueue: [],
				tickSign: 0
			});
		});
	}

	initTicker(symDataObj) {
		let symbol = symDataObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		let tickHistory = symDataObj.data.slice(-MAX_TICKER_LENGTH);
		if (!tickHistory.length) return false;
		tickHistory = tickHistory.reverse();
		let currResult;
		chan.tickerQueue = tickHistory.map(i => {
			currResult = i[5];
			return currResult *= getResultSign(symbol, currResult);
		});
	}

	updateTicker(quoteData) {
		let symbol = quoteData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return;
		this.serverTs = quoteData.time;
		let tickerQueue = chan.tickerQueue,
			result = quoteData.change,
			sign = getResultSign(symbol, result);
		result *= sign;
		if (tickerQueue.length >= MAX_TICKER_LENGTH) tickerQueue.pop();
		if (result !== undefined) tickerQueue.unshift(result);
		chan.tickerQueue = tickerQueue;
		chan.tickSign = sign;
	}
}

export default alt.createStore(TickerStore, 'TickerStore');
