'use strict';

import Actions from '../actions/actions';

class AppStore {
	constructor() {
		this.state = {
			inited: false,
			user: {},
			symbols: [],
			appMode: 'bx',
			activeSymbol: null,
			online: true
		};
		this.bindAction(Actions.initApp, this.initApp);
		this.bindAction(Actions.setActiveSymbol, this.setActiveSymbol);
		this.bindAction(Actions.userLoggedIn, this.userLoggedIn);
		this.bindAction(Actions.openTrade, this.openTrade);
		this.bindAction(Actions.updateBalance, this.updateBalance);
	}

	initApp(appSymbols) {
		if (this.state.inited) return;
		this.setState({
			inited: true,
			symbols: appSymbols,
			activeSymbol: appSymbols[0].symbol
		});
	}

	setActiveSymbol(sym) {
		if(sym === this.state.activeSymbol) return false;
		this.setState({
			activeSymbol: sym
		});
	}

	userLoggedIn(userData) {
		if (userData && userData.username && userData.userbalance) {
			this.setState({user: {username: userData.username, userbalance: Number(userData.userbalance)}});
		} else {
			this.setState({user: {}});
		}
	}

	openTrade(tradeData) {
		let user = JSON.parse(JSON.stringify(this.state.user)),
			betAmount = parseFloat(tradeData.amount);
		user.userbalance -= betAmount;
		this.setState({user});
	}

	updateBalance(profitLoss) {
		let user = JSON.parse(JSON.stringify(this.state.user)),
			tradeResult = parseFloat(profitLoss);
		user.userbalance += tradeResult;
		this.setState({user});
	}
}

export default alt.createStore(AppStore, 'AppStore');
