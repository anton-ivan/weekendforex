'use strict';

export default function getStylesheetRule(selector) {
	let ruleList = document.styleSheets[0].cssRules;
	for (let i = 0, z = ruleList.length; i < z; i++) {
		if (ruleList[i].type == CSSRule.STYLE_RULE && ruleList[i].selectorText == selector) {
			return ruleList[i].style;
		}
	}
	return null;
}
