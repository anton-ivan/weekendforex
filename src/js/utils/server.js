var io = require('socket.io-client');
var config = {
    //server_host: '//52.17.230.70:7080',
    //server_sslhost: '//52.17.230.70:7080'
    server_host: '//52.19.65.78:7080',
    server_sslhost: '//52.19.65.78:7080'
};

var serverClass = function() {
    var self = this;
    var host = config.server_host;
    var sslHost = config.server_sslhost;
    var socket;
    var sockmsg = [];
    var secure = (location.protocol == 'https:');
    var processServData = function (data) {
		    //console.log(data)
//    return;
        if (typeof data.type === 'undefined') {
            return false;
        }
        if (data.data === '[{undefined]') {
            console.error("Push from server with error data:" + data["data"]);
            return false;
        }
        //var d = msgpack.decode( data.data );
        var d = JSON.parse(data.data);
        self.onServerData(d, data.type);
    };
    var msgById = function (id) {
        for (var k in sockmsg) {
            if (sockmsg[k].msgid == id) {
                return k;
            }
        }
        return -1;
    };
    this.msgStructById = function (msgid) {
        for (var k in sockmsg) {
            if (sockmsg[k].msgid == msgid) {
                return sockmsg[k];
            }
        }
        return undefined;
    };
    var getNewId = function () {
        var id = (new Date()).getTime();
        while (msgById(id) !== -1)
            id++;
        return id;
    };
    this.connect = function () {
        if (typeof io === 'undefined') {
            throw new Error('Socket.IO is not defined');
        }
        socket = io.connect(secure ? sslHost : host, {
            'connect timeout': 500,
            'reconnect': true,
            'reconnection delay': 500,
            'reopen delay': 500,
            'max reconnection attempts': 10,
            'secure': secure
        });
        socket.on('a', function (a) {
            //console.log('server.res', a);
            if (a.msgid == undefined) {
                return processServData(a);
            }
            var id = msgById(parseInt(a.msgid));
            if (id != -1) {
                self.onResponse(a);
                if (sockmsg[id].callback) {
                    //sockmsg[id].callback( msgpack.decode( a.data ) );
                    sockmsg[id].callback(JSON.parse(a.data));
                }
                if (sockmsg[id].exp > 0) {
                    localStorage.setItem('nax_server_cache_' + sockmsg[id].type + '_' + JSON.stringify(sockmsg[id].data), JSON.stringify({
                        exp: sockmsg[id].exp,
                        data: JSON.parse(a.data)
                    }));
                } else if (sockmsg[id].ttl > 0) {
                    localStorage.setItem('nax_server_cache_' + sockmsg[id].type + '_' + JSON.stringify(sockmsg[id].data), JSON.stringify({
                        exp: (new Date()).getTime() + (sockmsg[id].ttl * 1000),
                        data: JSON.parse(a.data)
                    }));
                }
                delete sockmsg[id];
                return;
            }
        });
        socket.on('connect', function () {
            //console.log('onconnect');
            self.onConnect();
        });
        return true;
    };
    this.disconnect = function () {
        socket.disconnect();
    };
    this.send = function (type, data, callback, ttl, exp) {
        data = data || {};
        ttl = ttl || 0;
        exp = exp || 0;
        callback = callback || false;
        var now = Date.now();
        var msg = {type: type, msgid: getNewId(), data: data};
        //console.log('server.req', msg);
        if (socket != undefined) {
            var cached = localStorage.getItem('nax_server_cache_' + type + '_' + JSON.stringify(data));
            if (cached == undefined) {
                socket.emit('d', msg);
                msg.callback = callback;
                msg.ttl = ttl;
                msg.exp = exp;
                sockmsg.push(msg);
                self.onRequest(msg);
            } else {
                cached = JSON.parse(cached);
                if (cached['exp'] > now) {
                    self.onRequest(msg);
                    msg['data'] = cached['data'];
                    self.onResponse(msg);
                    if (callback) {
                        callback(msg['data']);
                    }
                } else {
                    socket.emit('d', msg);
                    msg.callback = callback;
                    sockmsg.push(msg);
                    self.onRequest(msg);
                }
            }
        }
    };
    // EVENTS LISTENERS
    var onRequestListeners = [];
    var onResponseListeners = [];
    var onConnectListeners = [];
    var onServerDataListeners = [];
    this.onRequest = function (msg) {
        for (var n in onRequestListeners) {
            if (onRequestListeners[n] != undefined) {
                onRequestListeners[n](msg);
            }
        }
    };
    this.subOnRequest = function (func) {
        onRequestListeners.push(func);
        return onRequestListeners.length - 1;
    };
    this.unsubOnRequest = function (index) {
        if (typeof onRequestListeners[index] !== 'undefined') {
            onRequestListeners[index] = undefined;
        }
    };
    this.onResponse = function (data) {
        for (var n in onResponseListeners) {
            if (onResponseListeners[n] != undefined) {
                onResponseListeners[n](data);
            }
        }
    };
    this.subOnResponse = function (func) {
        onResponseListeners.push(func);
        return onResponseListeners.length - 1;
    };
    this.unsubOnResponse = function (index) {
        if (typeof onResponseListeners[index] !== 'undefined') {
            onResponseListeners[index] = undefined;
        }
    };
    this.onConnect = function () {
        for (var n in onConnectListeners) {
            if (onConnectListeners[n] != undefined) {
                onConnectListeners[n]();
            }
        }
    };
    this.subOnConnect = function (func) {
        onConnectListeners.push(func);
        return onConnectListeners.length - 1;
    };
    this.unsubOnConnect = function (index) {
        //console.log('unsubOnConnect', index)
        if (typeof onConnectListeners[index] !== 'undefined') {
            onConnectListeners[index] = undefined;
        }
    };
    this.onServerData = function (data, type) {
		    //console.log('onServerData: ',type, data[0])
        for (var n in onServerDataListeners) {
            if (onServerDataListeners[n] != undefined) {
                onServerDataListeners[n](data, type);
            }
        }
    };
    this.subOnServerData = function (func) {
        onServerDataListeners.push(func);
	    //console.log(func,onServerDataListeners.length)
		    return onServerDataListeners.length - 1;
    };
    this.unsubOnServerData = function (index) {
		    //console.log('unsubOnServerData',index)
        if (typeof onServerDataListeners[index] !== 'undefined') {
            onServerDataListeners[index] = undefined;
        }
    }
}

//module.exports = serverClass;
export default new serverClass();
