'use strict';

export default function isEqual(d1, d2) {
	var equal = true,
		keys;

	if (d1 === d2) return true;

	if (typeof d1 !== typeof d2) return false;

	keys = Object.keys(d1).sort();
	if (keys.join() !== Object.keys(d2).sort().join()) return false;

	keys.forEach(function (key) {
		if (typeof d1[key] === 'object') {
			if (!isEqual(d1[key], d2[key])) {
				equal = false;
				return false;
			}
		} else {
			if (d1[key] !== d2[key]) {
				equal = false;
				return false;
			}
		}
	});
	return equal;
}
