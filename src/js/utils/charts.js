defaultSettings[ 'chartsUI' ] = {
  charts      : [
    {
      symbol : 'USDEUR',
      frame  : 'H1'
    }, {
      symbol : 'CHFUSD',
      frame  : 'M1'
    }, {
      symbol : 'EURJPY',
      frame  : 'H8'
    }
  ],
  drawColor   : '#B573F5',
  chartType   : 'candlestick',
  chartPeriod : 'H1',
  chartId     : 0
};
var chartsUIClass = function() {
  var chartOptions = {
    credits       : {
      enabled : false
    },
    chart         : {
      marginRight     : 48,
      renderTo        : 'graph',
      animation       : false,
      backgroundColor : 'rgba(49, 49, 49, 0)'
    },
    colors        : [
      '#007CC3'
    ],
    plotOptions   : {
      candlestick : {
        lineColor : '#666666',
        color     : '#FF4D4D',
        upColor   : '#62B600'
      }
    },
    rangeSelector : {
      enabled : false
    },
    title         : {
      text : 'Title'
    },
    tooltip       : {
      borderColor : '#007CC3',
      style       : {
        color : '#666666'
      }
    },
    navigation    : {
      buttonOptions : {
        enabled : false
      }
    },
    navigator     : {
      series : {
        dataGrouping : {
          forced : true
        },
        type         : 'areaspline',
        fillColor    : {
          linearGradient : {
            x1 : 0,
            y1 : 0,
            x2 : 0,
            y2 : 1
          },
          stops          : [
            [ 0, 'rgba(0,124,195,0.7)' ], [ 1, 'rgba(0,124,195,0.2)' ]
          ]
        },
        color        : '#007CC3',
        lineWidth    : 0
      }
    },
    series        : [
      {
        name         : 'Title',
        dataGrouping : {
          forced : true
        }
      }
    ]
  };
  var orderDrawingTemplate = {
    width     : 1,
    dashStyle : 'LongDash',
    label     : {
      style : {
        fontSize : '10px'
      },
      align : 'right',
      y     : 3,
      x     : 47
    },
    metaType  : 'hor-line'
  };
  var chartTypes = {
    M1  : [
      [ 'minute', [ 1 ] ]
    ],
    M5  : [
      [ 'minute', [ 5 ] ]
    ],
    M15 : [
      [ 'minute', [ 15 ] ]
    ],
    M30 : [
      [ 'minute', [ 30 ] ]
    ],
    H1  : [
      [ 'hour', [ 1 ] ]
    ],
    H4  : [
      [ 'hour', [ 4 ] ]
    ],
    H8  : [
      [ 'hour', [ 8 ] ]
    ],
    D1  : [
      [ 'day', [ 1 ] ]
    ],
    W1  : [
      [ 'week', [ 1 ] ]
    ],
    MN  : [
      [ 'month', [ 1 ] ]
    ]
  };
  var chartsUISettings;      //
  var chart;
  var chartPeriods = {
    M1  : 1,
    M5  : 5,
    M15 : 15,
    M30 : 30,
    H1  : 60,
    H4  : 240,
    H8  : 480,
    D1  : 1440,
    W1  : 10080,
    MN  : 44640
  };
  var chartSymbol;
  var chartId;    //
  var charts;
  var chartRenders = {};
  var chartOrders = {};
  var indicators = [];
  var indicatorWindows = 0;
  var noChart = true;
  var quotesLock = false;
  var drawFlag, drawStart;
  var drawColor; //
  var chartType; //
  var chartPeriod; //
  var chartLastUpdate;
  var chartNewPoint;
  var currentDiv = $( '#charts' );
  var chartContainer = $( '#graph' );
  var chartTabs = $( '#chart-tabs' );
  var updateChartPrice = function( quotes ) {
  };
  var subscrOnQuotesId = -1;
  var redrawChart = function() {
    if ( noChart || chartSymbol == undefined ) {
      return;
    }
    var opt = $.extend( true, {}, chartOptions );
    NAX.server.send( 'get_chart', {
      symbol : chartSymbol,
      frame  : chartPeriod,
      length : 200
    }, function( data ) {
      if ( noChart || chartSymbol == undefined ) {
        return;
      }
      if ( data.length == 0 ) {
        return;
      }
      var ch_data_size = data.length;
      for ( var i = ch_data_size; i--; ) {
        data[ i ][ 0 ] = NAX.market.fromServerToLocalTime( data[ i ][ 0 ] )
      }
      var period = 1000 * 60 * chartPeriods[ chartPeriod ];
      opt.chart.events = {
        load   : function() {
          this.xAxis[ 0 ].setExtremes( data[ ch_data_size > 40 ? ch_data_size - 40 : 0 ][ 0 ], data[ ch_data_size -
          1 ][ 0 ] );
          var series = this.series[ 0 ];
          updateChartPrice = function( e ) {
            if ( quotesLock ) {
              return;
            }
            for ( var n in e ) {
              if ( e[ n ].symbol != chartSymbol ) {
                continue;
              }
              var f_bid = parseFloat( e[ n ].bid );
              var t = (new Date()).getTime() + NAX.market.timeShift;
              if ( chartLastUpdate == undefined ) {
                chartLastUpdate = t;
                return;
              }
              if ( chartNewPoint[ 0 ] == undefined ) {
                chartNewPoint = [ f_bid, f_bid, f_bid, f_bid ];
              } else {
                chartNewPoint[ 3 ] = f_bid;
                if ( chartNewPoint[ 1 ] < f_bid ) {
                  chartNewPoint[ 1 ] = f_bid;
                }
                if ( chartNewPoint[ 2 ] > f_bid ) {
                  chartNewPoint[ 2 ] = f_bid;
                }
              }
            }
            if ( f_bid == undefined ) {
              return;
            }
            if ( chartLastUpdate == undefined || t - chartLastUpdate > 1000 ) {
              if ( chartNewPoint.length == 5 ) {
                chartNewPoint = [];
                return;
              }
              chartNewPoint.splice( 0, 0, t );
              series.yAxis.removePlotLine( 999 );
              series.yAxis.addPlotLine( {
                value     : f_bid,
                width     : 1,
                id        : 999,
                color     : '#c0c0c0',
                dashStyle : 'Solid',
                label     : {
                  style : {
                    color    : '#666',
                    fontSize : '10px'
                  },
                  text  : NAX.market.formatQuote( f_bid, chartSymbol ),
                  align : 'right',
                  y     : 3,
                  x     : 47
                }
              } );
              if ( charts[ chartId ].indicators != undefined && charts[ chartId ].indicators.length > 0 ) {
              }
              var pts = chart.series[ 0 ].cropStart + chart.series[ 0 ].processedXData.length;
              series.addPoint( chartNewPoint, true );
              if ( chart.series[ 0 ].cropStart + chart.series[ 0 ].processedXData.length - pts > 0 &&
                charts[ chartId ].indicators != undefined && charts[ chartId ].indicators.length > 0 ) {
                for ( var i in charts[ chartId ].indicators ) {
                  updateIndicator( charts[ chartId ].indicators[ i ] );
                }
                chart.redraw();
              }
              chartLastUpdate = t;
              chartNewPoint = [];
            }
          };
          if ( subscrOnQuotesId != -1 ) {
            NAX.market.unsubOnQuotes( subscrOnQuotesId );
          }
          subscrOnQuotesId = NAX.market.subOnQuotes( updateChartPrice );
          quotesLock = false;
          series.yAxis.removePlotLine( 999 );
          series.yAxis.addPlotLine( {
            value     : data[ ch_data_size - 1 ][ 4 ],
            width     : 1,
            id        : 999,
            color     : '#c0c0c0',
            dashStyle : 'Solid',
            label     : {
              style : {
                color    : '#666',
                fontSize : '10px'
              },
              text  : NAX.market.formatQuote( data[ ch_data_size - 1 ][ 4 ], chartSymbol ),
              align : 'right',
              y     : 3,
              x     : 47
            }
          } );
          if ( charts[ chartId ].draw == undefined ) {
            return;
          }
          for ( var i = 0; charts[ chartId ].draw[ i ] != undefined; i++ ) {
            var drawing = charts[ chartId ].draw[ i ];
            if ( drawing.metaType == 'hor-line' ) {
              series.yAxis.addPlotLine( drawing );
            }
            if ( drawing.metaType == 'vert-line' ) {
              series.xAxis.addPlotLine( drawing );
            }
          }
        },
        click  : function( e ) {
          tryDraw( e );
        },
        redraw : function( e ) {
          if ( chart == undefined || charts[ chartId ] == undefined || charts[ chartId ].draw == undefined ||
            chart.renderer == undefined ) {
            return;
          }
          for ( var i = 0; charts[ chartId ].draw[ i ] != undefined; i++ ) {
            var drawing = charts[ chartId ].draw[ i ];
            if ( drawing.metaType == 'hor-line' || drawing.metaType == 'vert-line' ) {
              continue;
            }
            if ( chartRenders[ i ] != undefined ) {
              chartRenders[ i ].destroy();
              delete chartRenders[ i ];
            }
            if ( drawing.metaType == 'beam-start' || drawing.metaType == 'segment-end' ) {
              chartRenders[ i ] = chart.renderer.circle( chart.xAxis[ 0 ].translate( drawing.chartX ) +
              10, chartContainer.height() - chart.yAxis[ 0 ].translate( drawing.chartY ) -
              94, drawing.radius ).attr( { fill : drawing.color } ).add();
              continue;
            }
            if ( drawing.metaType == 'beam-end' ) {
              var drawingFrom = charts[ chartId ].draw[ drawing.metaFrom ];
              var intersect = findIntersection( 10, chartContainer.height() - 94, chartContainer.width() -
              47, 40, chart.xAxis[ 0 ].translate( drawingFrom.chartX ) + 10, chartContainer.height() -
              chart.yAxis[ 0 ].translate( drawingFrom.chartY ) - 94, chart.xAxis[ 0 ].translate( drawing.metaToX ) +
              10, chartContainer.height() - chart.yAxis[ 0 ].translate( drawing.metaToY ) - 94 );
              chartRenders[ i ] = chart.renderer.path( [
                'M', chart.xAxis[ 0 ].translate( drawingFrom.chartX ) + 10,
                chartContainer.height() - chart.yAxis[ 0 ].translate( drawingFrom.chartY ) - 94, 'L', intersect.x,
                intersect.y
              ] ).attr( {
                'stroke-width' : 1,
                stroke         : drawingFrom.color
              } ).add();
            }
            if ( drawing.metaType == 'segment' ) {
              var drawingFrom = charts[ chartId ].draw[ drawing.metaFrom ];
              chartRenders[ i ] = chart.renderer.path( [
                'M', chart.xAxis[ 0 ].translate( drawingFrom.chartX ) + 10,
                chartContainer.height() - chart.yAxis[ 0 ].translate( drawingFrom.chartY ) - 94, 'L',
                chart.xAxis[ 0 ].translate( drawing.metaToX ) + 10,
                chartContainer.height() - chart.yAxis[ 0 ].translate( drawing.metaToY ) - 94
              ] ).attr( {
                'stroke-width' : 1,
                stroke         : drawingFrom.color
              } ).add();
            }
          }
          return;
        }
      };
      opt.title.text = getSymbolName( chartSymbol, "none" ) + ' ' + getToken( chartPeriod.toLowerCase(), null, "none" );
      opt.navigator.series.data = data;
      opt.navigator.series.dataGrouping.units = chartTypes[ chartPeriod ];
      opt.series[ 0 ].type = chartType;
      opt.series[ 0 ].name = chartSymbol;
      opt.series[ 0 ].data = data;
      opt.series[ 0 ].dataGrouping.units = chartTypes[ chartPeriod ];
      opt.yAxis = {
        labels : {
          formatter : function() {
            return NAX.market.formatQuote( this.value, chartSymbol );
          }
        }
      };
      if ( chart != undefined ) {
        chart.destroy();
        delete chart;
        chartRenders = {};
      }
      chartLastUpdate = undefined;
      chartNewPoint = [];
      Highcharts.setOptions( {                                            // This is for all plots, change Date axis to local timezone
        global : {
          useUTC : false
        }
      } );
      chart = new Highcharts.StockChart( opt );
      NAX.market.subscribe( chartSymbol );
      drawOrders();
      if ( chartType != 'line' ) { // Don't have some data for build indicator
        drawIndicators();
      }
    } );
  };
  var tryDraw = function( e ) {
    if ( drawFlag == undefined ) {
      return;
    }
    switch ( drawFlag ) {
      case 'hor-line':
        var drawId = getNewDrawId();
        if ( drawId == -1 ) {
          return;
        }
        var drawing = {
          value     : e.yAxis[ 0 ].value,
          width     : 1,
          id        : drawId,
          color     : drawColor,
          dashStyle : 'Solid',
          label     : {
            style : {
              color    : drawColor,
              fontSize : '10px'
            },
            text  : NAX.market.formatQuote( e.yAxis[ 0 ].value, chartSymbol ),
            align : 'right',
            y     : 3,
            x     : 47
          },
          metaType  : 'hor-line'
        };
        chart.series[ 0 ].yAxis.addPlotLine( drawing );
        charts[ chartId ].draw[ drawId ] = drawing;
        break;
      case 'vert-line':
        var drawId = getNewDrawId();
        if ( drawId == -1 ) {
          return;
        }
        var drawing = {
          value     : e.xAxis[ 0 ].value,
          width     : 1,
          id        : drawId,
          color     : drawColor,
          dashStyle : 'Solid',
          label     : {
            style         : {
              color    : drawColor,
              fontSize : '11px'
            },
            text          : toChartTime( e.xAxis[ 0 ].value ),
            verticalAlign : 'bottom',
            align         : 'center',
            rotation      : 0,
            y             : 25,
            x             : 0
          },
          metaType  : 'vert-line'
        };
        chart.series[ 0 ].xAxis.addPlotLine( drawing );
        charts[ chartId ].draw[ drawId ] = drawing;
        break;
      case 'beam':
        var drawId = getNewDrawId();
        if ( drawId == -1 ) {
          return;
        }
        if ( drawStart == undefined ) {
          drawStart = e;
          drawStart.drawing = {
            chartX   : e.xAxis[ 0 ].value,
            chartY   : e.yAxis[ 0 ].value,
            radius   : 2,
            color    : drawColor,
            metaType : 'beam-start'
          }
          chartRenders[ drawId ] = chart.renderer.circle( e.chartX, e.chartY, drawStart.drawing.radius ).attr( { fill : drawStart.drawing.color } ).add();
          return;
        }
        charts[ chartId ].draw[ drawId ] = drawStart.drawing;
        var drawing = {
          color    : drawColor,
          metaType : 'beam-end',
          metaFrom : drawId,
          metaToX  : e.xAxis[ 0 ].value,
          metaToY  : e.yAxis[ 0 ].value
        }
        drawId++;
        var intersect = findIntersection( 10, chartContainer.height() - 94, chartContainer.width() -
        45, 40, drawStart.chartX, drawStart.chartY, e.chartX, e.chartY );
        chartRenders[ drawId ] = chart.renderer.path( [
          'M', drawStart.chartX, drawStart.chartY, 'L', intersect.x, intersect.y
        ] ).attr( {
          'stroke-width' : 1,
          stroke         : drawStart.drawing.color
        } ).add();
        charts[ chartId ].draw[ drawId ] = drawing;
        drawStart = undefined;
        break;
      case 'segment':
        var drawId = getNewDrawId();
        if ( drawId == -1 ) {
          return;
        }
        if ( drawStart == undefined ) {
          drawStart = e;
          drawStart.drawing = {
            chartX   : e.xAxis[ 0 ].value,
            chartY   : e.yAxis[ 0 ].value,
            radius   : 2,
            color    : drawColor,
            metaType : 'segment-end'
          }
          chartRenders[ drawId ] = chart.renderer.circle( e.chartX, e.chartY, drawStart.drawing.radius ).attr( { fill : drawStart.drawing.color } ).add();
          return;
        }
        charts[ chartId ].draw[ drawId ] = drawStart.drawing;
        var drawing = {
          color    : drawColor,
          metaType : 'segment',
          metaFrom : drawId,
          metaToX  : e.xAxis[ 0 ].value,
          metaToY  : e.yAxis[ 0 ].value
        }
        drawId++;
        chartRenders[ drawId ] = chart.renderer.path( [
          'M', drawStart.chartX, drawStart.chartY, 'L', e.chartX, e.chartY
        ] ).attr( {
          'stroke-width' : 1,
          stroke         : drawStart.drawing.color
        } ).add();
        charts[ chartId ].draw[ drawId ] = drawing;
        drawStart = undefined;
        drawId++;
        drawing = {
          chartX   : e.xAxis[ 0 ].value,
          chartY   : e.yAxis[ 0 ].value,
          radius   : 2,
          color    : drawColor,
          metaType : 'segment-end'
        }
        chartRenders[ drawId ] = chart.renderer.circle( e.chartX, e.chartY, drawing.radius ).attr( { fill : drawing.color } ).add();
        charts[ chartId ].draw[ drawId ] = drawing;
        break;
      default:
        return;
        break;
    }
  }
  var getNewDrawId = function() {
    if ( noChart || chartId == undefined ) {
      return -1;
    }
    if ( charts[ chartId ].draw == undefined || charts[ chartId ].draw[ 0 ] == undefined ) {
      charts[ chartId ].draw = [];
      return 0;
    }
    var i = 0;
    for ( i = 0; charts[ chartId ].draw[ i ] != undefined; i++ ) {
      ;
    }
    return i;
  }
  var findIntersection = function( bLeft, bTop, bRight, bBottom, startX, startY, endX, endY ) {
    var endX = startX + (endX - startX) * 100;
    var endY = startY + (endY - startY) * 100;
    var ret = {};
    // go right
    if ( endX > startX ) {
      ret.x = bRight;
      ret.y = startY + (endY - startY) * (bRight - startX) / (endX - startX);
      // go top and fix it
      if ( endY > startY && ret.y > bTop ) {
        ret.y = bTop;
        ret.x = startX + (endX - startX) * (bTop - startY) / (endY - startY);
      }
      // go down and fix it
      if ( endY < startY && ret.y < bBottom ) {
        ret.y = bBottom;
        ret.x = startX + (endX - startX) * (bBottom - startY) / (endY - startY);
      }
    }
    // go left
    if ( endX <= startX ) {
      ret.x = bLeft;
      ret.y = startY + (endY - startY) * (bLeft - startX) / (endX - startX);
      // go top and fix it
      if ( endY > startY && ret.y > bTop ) {
        ret.y = bTop;
        ret.x = startX + (endX - startX) * (bTop - startY) / (endY - startY);
      }
      // go down and fix it
      if ( endY < startY && ret.y < bBottom ) {
        ret.y = bBottom;
        ret.x = startX + (endX - startX) * (bBottom - startY) / (endY - startY);
      }
    }
    return ret;
  }
  var enableDraw = function( e, use ) {
    use = false || use;
    if ( !use ) {
      if ( e.closest( '.float-grey-btn' ).hasClass( 'active' ) ) {
        e.closest( '.float-grey-btn' ).removeClass( 'active' );
        drawFlag = undefined;
      } else {
        $( '.draw-tool' ).closest( '.float-grey-btn' ).removeClass( 'active' );
        e.closest( '.float-grey-btn' ).addClass( 'active' );
        drawFlag = e.attr( 'id' ).substr( 5 );
      }
    } else {
      $( '.draw-tool' ).closest( '.float-grey-btn' ).removeClass( 'active' );
      e.closest( '.float-grey-btn' ).addClass( 'active' );
      drawFlag = e.attr( 'id' ).substr( 5 );
    }
    if ( drawStart != undefined ) {
      var drId = getNewDrawId();
      chartRenders[ drId ].destroy();
      chartRenders.splice( drId, 1 );
      drawStart = undefined;
    }
  }
  var undoDraw = function() {
    if ( noChart ) {
      return;
    }
    if ( drawStart != undefined ) {
      var drId = getNewDrawId();
      chartRenders[ drId ].destroy();
      delete chartRenders[ drId ];
      drawStart = undefined;
      return;
    }
    var drId = getNewDrawId() - 1;
    if ( drId < 0 || charts[ chartId ].draw[ drId ] == undefined ) {
      return;
    }
    if ( chartRenders[ drId ] != undefined ) {
      chartRenders[ drId ].destroy();
      delete chartRenders[ drId ];
    }
    if ( charts[ chartId ].draw[ drId ].metaType == 'beam-end' ) {
      charts[ chartId ].draw.splice( drId, 1 );
      drId--;
      if ( chartRenders[ drId ] != undefined ) {
        chartRenders[ drId ].destroy();
        delete chartRenders[ drId ];
      }
      charts[ chartId ].draw.splice( drId, 1 );
      return;
    }
    if ( charts[ chartId ].draw[ drId ].metaType == 'segment-end' ) {
      charts[ chartId ].draw.splice( drId, 1 );
      for ( var i = 1; i < 3; i++ ) {
        if ( chartRenders[ drId - i ] != undefined ) {
          chartRenders[ drId - i ].destroy();
          delete chartRenders[ drId - i ];
        }
        charts[ chartId ].draw.splice( drId - i, 1 );
      }
      return;
    }
    if ( charts[ chartId ].draw[ drId ].metaType == 'hor-line' ) {
      chart.series[ 0 ].yAxis.removePlotLine( drId );
      charts[ chartId ].draw.splice( drId, 1 );
      return;
    }
    if ( charts[ chartId ].draw[ drId ].metaType == 'vert-line' ) {
      chart.series[ 0 ].xAxis.removePlotLine( drId );
      charts[ chartId ].draw.splice( drId, 1 );
      return;
    }
  }
  var clearDraw = function() {
    if ( noChart ) {
      return;
    }
    if ( drawStart != undefined ) {
      drawStart = undefined;
    }
    if ( charts[ chartId ].draw != undefined ) {
      for ( var i = 0; charts[ chartId ].draw[ i ] != undefined; i++ ) {
        if ( chartRenders[ i ] != undefined ) {
          chartRenders[ i ].destroy();
        }
        if ( charts[ chartId ].draw[ i ].metaType == 'hor-line' ) {
          chart.series[ 0 ].yAxis.removePlotLine( i );
        }
        if ( charts[ chartId ].draw[ i ].metaType == 'vert-line' ) {
          chart.series[ 0 ].xAxis.removePlotLine( i );
        }
      }
    }
    chartRenders = [];
    charts[ chartId ].draw = [];
  };
  var drawOrders = function( clear ) {
    clear = clear || false;
    if ( noChart == true ) {
      return;
    }
    for ( var k in chartOrders ) {
      chart.series[ 0 ].yAxis.removePlotLine( chartOrders[ k ] );
      delete chartOrders[ k ];
    }
    if ( clear ) {
      return;
    }
    if ( NAX.trade.trades != undefined ) {
      for ( var k in NAX.trade.trades ) {
        if ( NAX.trade.trades[ k ].symbol != chartSymbol ) {
          continue;
        }
        var drawing = $.extend( true, {}, orderDrawingTemplate );
        var color = NAX.trade.trades[ k ].flags & NAX.trade.ORDER_PENDING ? (NAX.trade.trades[ k ].flags &
        NAX.trade.ORDER_BUY ? '#ACE1FF' : '#FFADAD') : (NAX.trade.trades[ k ].flags &
        NAX.trade.ORDER_BUY ? '#51A5D6' : '#FF4D4D');
        var value = NAX.trade.trades[ k ].flags &
        NAX.trade.ORDER_PENDING ? NAX.trade.trades[ k ].price_open1 : NAX.trade.trades[ k ].price_open;
        drawing.value = value;
        drawing.id = k * 3 + 1;
        drawing.color = color;
        drawing.label.text = NAX.market.formatQuote( value, chartSymbol );
        drawing.label.style.color = color;
        chart.series[ 0 ].yAxis.addPlotLine( drawing );
        chartOrders[ 'op' + NAX.trade.trades[ k ].order ] = drawing.id;
        if ( !(NAX.trade.trades[ k ].flags & NAX.trade.ORDER_PENDING) && NAX.trade.trades[ k ].sl > 0 ) {
          var drawing = $.extend( true, {}, orderDrawingTemplate );
          drawing.value = NAX.trade.trades[ k ].sl;
          drawing.id = k * 3 + 2;
          drawing.color = '#ABABAB';
          drawing.label.text = NAX.market.formatQuote( NAX.trade.trades[ k ].sl, chartSymbol );
          drawing.label.style.color = '#ABABAB';
          chart.series[ 0 ].yAxis.addPlotLine( drawing );
          chartOrders[ 'sl' + NAX.trade.trades[ k ].order ] = drawing.id;
        }
        if ( !(NAX.trade.trades[ k ].flags & NAX.trade.ORDER_PENDING) && NAX.trade.trades[ k ].tp > 0 ) {
          var drawing = $.extend( true, {}, orderDrawingTemplate );
          drawing.value = NAX.trade.trades[ k ].tp;
          drawing.id = k * 3 + 3;
          drawing.color = '#ABABAB';
          drawing.label.text = NAX.market.formatQuote( NAX.trade.trades[ k ].tp, chartSymbol );
          drawing.label.style.color = '#ABABAB';
          chart.series[ 0 ].yAxis.addPlotLine( drawing );
          chartOrders[ 'tp' + NAX.trade.trades[ k ].order ] = drawing.id;
        }
      }
    }
  }
  var drawIndicators = function() {
    if ( noChart == true || chartId == undefined || charts[ chartId ] == undefined ) {
      return;
    }
    indicatorWindows = 0;
    if ( charts[ chartId ].indicators == undefined ) {
      return;
    }
    var newIndicators = charts[ chartId ].indicators;
    charts[ chartId ].indicators = [];
    for ( var i = 0, l = newIndicators.length; i < l; i++ ) {
      chartsUI.addIndicator( newIndicators[ i ].name, newIndicators[ i ].params, newIndicators[ i ].separate, newIndicators[ i ].colors );
    }
  };
  var checkChartsUISubscription = function( symbol ) {
    return chartSymbol == symbol;
  };
  var addTab = function( chart, select ) {
    if ( chart == undefined ) {
      return;
    }
    select = select || true;
    var body = $( '.list-body', chartTabs );
    var nItem = $( '<div data-symbol="' + chart.symbol + '" data-timeframe="' + chart.frame +
    '" class="node"><span class="name">' + getSymbolName( chart.symbol ) + ' (<span class="timeframe">' +
    getToken( chart.frame.toLowerCase() ) +
    '</span>)</span><span class="node-close"><i class="fa fa-times"></i></span></div>' );
    body.append( nItem );
    if ( select ) {
      selectChart( chartId );
    }
  }
  var selectChart = function( id ) {
    chartsUI && chartsUI.resizeTabs();
    if ( charts[ id ] == undefined ) {
      return;
    }
    quotesLock = true;
    chartId = id;
    chartSymbol = charts[ chartId ].symbol;
    chartPeriod = charts[ chartId ].frame;
    $( '.node:eq(' + chartId + ')', chartTabs ).addClass( 'active' ).siblings().removeClass( 'active' );
    $( '#time-period', currentDiv ).val( chartPeriod ).trigger( "chosen:updated" );
    redrawChart();
  }
  var deleteChart = function( id ) {
    if ( chartId == id ) {
      var size = charts.length - 1;
      quotesLock = true;
      chartId = id == size ? size - 1 : id;
      if ( chartId == -1 ) {
        chartId = undefined;
        noChart = true;
        chart.destroy();
        chart = undefined;
        chartRenders = {};
      }
      charts.splice( id, 1 );
      selectChart( chartId );
    } else {
      charts.splice( id, 1 );
      if ( id < chartId ) {
        chartId--;
      }
    }
  }
  var applySettings = function() {
    //clear
    $( '.list-body', chartTabs ).empty();
    $( '.button-wrapper > .icon_button:not(.draw-undo, .draw-clear)', currentDiv ).removeClass().addClass( 'icon_button' );
    if ( chart != undefined ) {
      chartId = undefined;
      noChart = true;
      quotesLock = true;
      chart.destroy();
      chart = undefined;
      chartRenders = {};
    }
    drawFlag = drawStart = chartLastUpdate = chartNewPoint = undefined;
    //get new settings
    chartsUISettings = settingsUI.get( 'chartsUI' );
    chartId = chartsUISettings.chartId;
    charts = chartsUISettings.charts;
    drawColor = chartsUISettings.drawColor;
    chartType = chartsUISettings.chartType;
    chartPeriod = chartsUISettings.chartPeriod;
    //apply settings
    for ( var n in charts ) {
      addTab( charts[ n ], false );
    }
    if ( charts.length ) {
      noChart = false;
      selectChart( chartId );
    } else {
      $( '#time-period', currentDiv ).val( chartPeriod ).trigger( "chosen:updated" );
    }
    var tab = $( '.js-tabs .item[data-chart-type="' + chartType + '"]', currentDiv );
    changeTab.apply( tab );
    var colorPalette = drawColor.substr( 1 );
    var paletteDiv = $( '#palette' );
    var nColor = $( '.list-wrp .icon-palette[data-value="' + colorPalette + '"]', paletteDiv ).clone();
    var _class = nColor.attr( 'class' ).split( ' ' )[ 1 ];
    paletteDiv.data( 'color', colorPalette ).data( 'class', _class ).find( '.palette-current' ).empty().append( nColor );
    resizeLayout();
  }
  var resizeLayout = function() {
    chartsUI.resizeChart();
  }
  this.resizeChart = function() {
    var body = chartContainer.closest( '.body' );
    var BH = body.height() - 40;
    if ( chartContainer.length ) {
      chartContainer.height( BH );
      if ( noChart == false ) {
        if ( chart ) {
          var contH = BH - 94 - chart.yAxis[ 0 ].top;
          var stdHeight = Math.round( (contH - (10 * (indicatorWindows == 0 ? 0 : indicatorWindows))) /
          (indicatorWindows + 2) );
          for ( var i = 0; i <= indicatorWindows + 1; i++ ) {
            if ( i == 1 ) {
              continue;
            }
            var opts = chart.yAxis[ i ].options;
            if ( i == 0 ) {
              opts.height = 2 * stdHeight;
            } else {
              opts.height = stdHeight;
              opts.top = chart.yAxis[ 0 ].top + (opts.height * i) + ((i - 1) * 10);
            }
            chart.yAxis[ i ].setOptions( opts );
          }
          chart.setSize( chartContainer.width(), chartContainer.height(), true );
        }
      }
    }
    chartsUI.resizeTabs();
  }
  this.resizeTabs = function() {
    chartTabs.off( 'mousedown' ).off( 'mouseup' );
    var wrp = $( '.list-wrp', chartTabs );
    var body = $( '.list-body', wrp );
    var bodyTabW = body.outerWidth( true );
    var itemWidth = 135;
    if ( bodyTabW ) {
      var bodyLeft = parseFloat( body.css( 'left' ).replace( 'px', '' ) );
      var wrpTabW = wrp.outerWidth();
      var diff = wrpTabW - bodyTabW;
      var vBodyW = bodyTabW + bodyLeft;
      if ( diff < 0 ) {
        if ( wrpTabW - vBodyW > 0 ) {
          body.css( 'left', diff );
        }
        chartTabs.addClass( 'show-arrow' );
        chartTabs.on( 'mousedown', '.arrow-right', function() {
          var l = parseFloat( body.css( 'left' ).replace( 'px', '' ) );
          if ( l <= diff ) {
            body.css( 'left', diff );
            return;
          }
          body.animate( { left : '-=' + itemWidth }, {
            duration : 1000,
            step     : function( now, obj ) {
              if ( now <= diff ) {
                body.stop().css( 'left', diff );
              }
            }
          } );
        } ).on( 'mouseup', '.arrow-right', function() {
          body.stop()
        } );
        chartTabs.on( 'mousedown', '.arrow-left', function() {
          var l = parseFloat( body.css( 'left' ).replace( 'px', '' ) );
          if ( l >= 0 ) {
            body.css( 'left', 0 );
            return;
          }
          body.animate( { left : '+=' + itemWidth }, {
            duration : 1000,
            step     : function( now, obj ) {
              if ( now >= 0 ) {
                body.stop().css( 'left', 0 );
              }
            }
          } );
        } ).on( 'mouseup', '.arrow-right', function() {
          body.stop()
        } );
        return;
      }
    }
    body.css( 'left', 0 );
    chartTabs.removeClass( 'show-arrow' );
  }
  this.changeChartType = function( type ) {
    chartType = type;
    redrawChart();
  }
  this.changeChartFrame = function( frame ) {
    chartPeriod = frame;
    if ( noChart ) {
      return;
    }
    charts[ chartId ].frame = chartPeriod;
    $( '.node.active', chartTabs ).data( 'timeframe', frame ).find( '.timeframe' ).text( frame );
    redrawChart();
  }
  this.addChart = function( symbol ) {
    chartSymbol = symbol;
    var new_chart = {
      symbol : chartSymbol,
      frame  : chartPeriod
    };
    var nId = charts.push( new_chart ) - 1;
    quotesLock = true;
    if ( noChart ) {
      noChart = false;
      chartId = 0;
    } else {
      chartId = nId
    }
    addTab( new_chart );
  };
  NAX.market.subOnCheckSubscription( checkChartsUISubscription );
  NAX.trade.subOnTradeUpdate( function() {
    drawOrders();
  } );
  settingsUI.subOnInit( applySettings );
  layoutUI.subOnEndResize( resizeLayout );
  layoutUI.subOnAllResize( resizeLayout );
  chartTabs.on( 'click', '.node:not(.active)', function() {
    var _this = $( this );
    var index = _this.index();
    selectChart( index );
  } );
  chartTabs.on( 'click', '.node-close', function() {
    var node = $( this ).parent( '.node' );
    var index = node.index();
    node.remove();
    deleteChart( index );
  } );
  currentDiv.on( 'click', '.js-tabs .item:not(.active)', function() {
    changeTab.apply( this );
    var _val = $( this ).data( 'chart-type' );
    chartsUI.changeChartType( _val );
  } );
  currentDiv.on( 'change', '#time-period', function() {
    var _val = $( this ).val();
    chartsUI.changeChartFrame( _val );
  } );
  currentDiv.on( 'change', '#palette', function() {
    var palette = $( this );
    drawColor = '#' + palette.data( 'color' );
    var button = $( '.icon_button.active[data-draw]', currentDiv );
    if ( button.length ) {
      var _class = 'icon_button active ' + palette.data( 'class' );
      button.removeClass().addClass( _class );
    }
  } );
  currentDiv.on( 'click', '.icon_button[data-draw]', function() {
    var _this = $( this );
    if ( _this.hasClass( 'active' ) ) {
      _this.removeClass().addClass( 'icon_button' );
      drawFlag = undefined;
    } else {
      var palette = $( '#palette' );
      var _class = palette.data( 'class' ) || palette.attr( 'data-class' );
      _class = 'icon_button active ' + _class;
      _this.addClass( _class ).siblings( '[data-draw]' ).removeClass().addClass( 'icon_button' );
      drawFlag = _this.data( 'draw' );
    }
  } );
  currentDiv.on( 'click', '.icon_button.draw-undo', function() {
    undoDraw();
  } );
  currentDiv.on( 'click', '.icon_button.draw-clear', function() {
    clearDraw();
  } );
  var getSeries = function() {
    var data = [];
    if ( chart == undefined || noChart == true ) {
      return data;
    }
    if ( chart.series[ 0 ].cropped ) {
      for ( var n = 0, l = chart.series[ 0 ].cropStart; n < l; n++ ) {
        data.push( {
          x     : chart.series[ 0 ].xData[ n ],
          open  : chart.series[ 0 ].yData[ n ][ 0 ],
          high  : chart.series[ 0 ].yData[ n ][ 1 ],
          low   : chart.series[ 0 ].yData[ n ][ 2 ],
          close : chart.series[ 0 ].yData[ n ][ 3 ]
        } )
      }
    }
    for ( n = 0, l = chart.series[ 0 ].groupedData.length; n < l; n++ ) {
      data.push( chart.series[ 0 ].groupedData[ n ] );
    }
    return data;
  }
  var indicatorSimpleMovingAverage = function( period, base ) {
    period = period || 14;
    period = parseInt( period );
    base = base || 'close';
    var indicator = {
      name  : 'SMA (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#b573f5'
    }
    var SMA = 0;
    var chartPoints = getSeries();
    if ( chartPoints.length < period ) {
      return [ indicator ];
    }
    for ( var n = 0, l = period; n < l; n++ ) {
      SMA += chartPoints[ n ][ base ];
    }
    SMA = SMA / period;
    for ( n = period, l = chartPoints.length; n < l; n++ ) {
      SMA = SMA + (chartPoints[ n ][ base ] / period) - (chartPoints[ n - period ][ base ] / period);
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( SMA, chartSymbol ) ) ] );
    }
    return [ indicator ];
  }
  var indicatorCumulativeMovingAverage = function( base ) {
    base = base || 'close';
    var indicator = {
      name  : 'CMA',
      data  : [],
      type  : 'spline',
      color : '#b573f5'
    }
    var chartPoints = getSeries();
    if ( !chartPoints.length ) {
      return [ indicator ];
    }
    var CMA = chartPoints[ 0 ][ base ];
    for ( n = 1, l = chartPoints.length; n < l; n++ ) {
      CMA = CMA + (chartPoints[ n ][ base ] - CMA) / n;
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( CMA, chartSymbol ) ) ] );
    }
    return [ indicator ];
  }
  var indicatorWeightedMovingAverage = function( period, base ) {
    period = period || 14;
    period = parseInt( period );
    base = base || 'close';
    var indicator = {
      name  : 'WMA (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#b573f5'
    }
    var points = [];
    var mult = 2 / (period * (period + 1));
    var chartPoints = getSeries();
    for ( var n = 0, l = period - 1; n < l; n++ ) {
      if ( chartPoints[ n ] == undefined ) {
        return [ indicator ];
      } else {
        points.push( chartPoints[ n ] );
      }
    }
    for ( n = period, l = chartPoints.length; n < l; n++ ) {
      points.push( chartPoints[ n ] );
      var _val = 0;
      for ( var i = points.length - 1; i >= 0; i-- ) {
        _val += (period - i) * chartPoints[ n - i ][ base ];
      }
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( mult * _val, chartSymbol ) ) ] );
      points.shift();
    }
    return [ indicator ];
  }
  var indicatorExponentialMovingAverage = function( period, base ) {
    period = period || 14;
    period = parseInt( period );
    base = base || 'close';
    var indicator = {
      name  : 'EMA (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#b573f5'
    }
    var alpha = 2 / (period + 1);
    var chartPoints = getSeries();
    if ( !chartPoints.length ) {
      return [ indicator ];
    }
    var EMA = chartPoints[ 0 ][ base ];
    indicator.data.push( [ chartPoints[ 0 ].x, parseFloat( NAX.market.formatQuote( EMA, chartSymbol ) ) ] );
    for ( var n = 1, l = chartPoints.length; n < l; n++ ) {
      EMA = alpha * chartPoints[ n ][ base ] + (1 - alpha) * EMA;
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( EMA, chartSymbol ) ) ] );
    }
    return [ indicator ];
  }
  var indicatorRelativeStrengthIndex = function( period ) {
    period = period || 14;
    period = parseInt( period );
    var indicator = {
      name  : 'RSI (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#ff4d4d'
    }
    var alpha = 2 / (period + 1);
    var chartPoints = getSeries();
    if ( !chartPoints.length || chartPoints.length < 2 ) {
      return [ indicator ];
    }
    var EMA_U = 0;
    var EMA_D = 0;
    for ( var n = 1, l = chartPoints.length; n < l; n++ ) {
      var dif = chartPoints[ n ][ 'close' ] - chartPoints[ n - 1 ][ 'close' ];
      if ( dif > 0 ) {
        EMA_U = alpha * dif + (1 - alpha) * EMA_U;
        EMA_D = (1 - alpha) * EMA_D;
      } else if ( dif < 0 ) {
        EMA_D = alpha * (-1 * dif) + (1 - alpha) * EMA_D;
        EMA_U = (1 - alpha) * EMA_U;
      } else {
        EMA_U = (1 - alpha) * EMA_U;
        EMA_D = (1 - alpha) * EMA_D;
      }
      indicator.data.push( [
        chartPoints[ n ].x, parseFloat( (EMA_D ? 100 - (100 / (1 + ( EMA_U / EMA_D))) : 100).toFixed( 4 ) )
      ] );
    }
    return [ indicator ];
  }
  var indicatorBollingerBands = function( period, base ) {
    period = period || 14;
    period = parseInt( period );
    base = base || 'close';
    var indicator = [
      {
        name  : 'BB (' + period + ')',
        data  : [],
        type  : 'spline',
        color : '#007cc3'
      }, {
        name  : 'BB High (' + period + ')',
        data  : [],
        type  : 'spline',
        color : '#007cc3'
      }, {
        name  : 'BB Low (' + period + ')',
        data  : [],
        type  : 'spline',
        color : '#007cc3'
      }
    ];
    var SMA = 0;
    var last = [];
    var chartPoints = getSeries();
    if ( chartPoints.length < period ) {
      return indicator;
    }
    for ( var n = 0, l = period; n < l; n++ ) {
      SMA += chartPoints[ n ][ base ];
      last.push( chartPoints[ n ][ base ] );
    }
    SMA = SMA / period;
    for ( n = period, l = chartPoints.length; n < l; n++ ) {
      last.shift();
      last.push( chartPoints[ n ][ base ] );
      var sigma = 0;
      for ( var i = 0; i < period; i++ ) {
        sigma += Math.pow( last[ i ] - SMA, 2 );
      }
      sigma = Math.sqrt( sigma / period );
      SMA = SMA + (chartPoints[ n ][ base ] / period) - (chartPoints[ n - period ][ base ] / period);
      indicator[ 0 ].data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( SMA, chartSymbol ) ) ] );
      indicator[ 1 ].data.push( [
        chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( SMA + (2 * sigma), chartSymbol ) )
      ] );
      indicator[ 2 ].data.push( [
        chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( SMA - (2 * sigma), chartSymbol ) )
      ] );
    }
    return indicator;
  }
  var indicatorMovingAverageConvergenceDivergence = function( period_fast, period_slow, period_signal, base ) {
    period_fast = period_fast || 12;
    period_slow = period_slow || 26;
    period_signal = period_signal || 9;
    period_fast = parseInt( period_fast );
    period_slow = parseInt( period_slow );
    period_signal = parseInt( period_signal );
    base = base || 'close';
    var indicator = [
      {
        name  : 'MACD (' + period_fast + ',' + period_slow + ')',
        data  : [],
        type  : 'spline',
        color : '#007cc3'
      }, {
        name      : 'MACD Signal (' + period_fast + ',' + period_slow + ',' + period_signal + ')',
        data      : [],
        type      : 'spline',
        dashStyle : 'Dash',
        color     : '#ff4d4d'
      }, {
        name : 'MACD Hist (' + period_fast + ',' + period_slow + ',' + period_signal + ')',
        data : [],
        type : 'column'
      }
    ];
    var alpha_fast = 2 / (period_fast + 1);
    var alpha_slow = 2 / (period_slow + 1);
    var chartPoints = getSeries();
    if ( chartPoints.length < period_signal ) {
      return indicator;
    }
    var EMA_fast = chartPoints[ 0 ][ base ];
    var EMA_slow = chartPoints[ 0 ][ base ];
    var MACD = EMA_fast - EMA_slow;
    var MACD_Signal = MACD;
    var MACD_arr = [ MACD ]
    for ( var n = 1, l = period_signal; n < l; n++ ) {
      EMA_fast = alpha_fast * chartPoints[ n ][ base ] + (1 - alpha_fast) * EMA_fast;
      EMA_slow = alpha_slow * chartPoints[ n ][ base ] + (1 - alpha_slow) * EMA_slow;
      MACD = EMA_fast - EMA_slow;
      MACD_arr[ n ] = MACD;
      MACD_Signal += MACD;
    }
    MACD_Signal = MACD_Signal / period_signal;
    var MACD_Hist = MACD - MACD_Signal;
    indicator[ 0 ].data.push( [ chartPoints[ 0 ].x, parseFloat( MACD.toFixed( 8 ) ) ] );
    indicator[ 1 ].data.push( [ chartPoints[ 0 ].x, parseFloat( MACD_Signal.toFixed( 8 ) ) ] );
    indicator[ 2 ].data.push( [ chartPoints[ 0 ].x, parseFloat( MACD_Hist.toFixed( 8 ) ) ] );
    for ( n = period_signal, l = chartPoints.length; n < l; n++ ) {
      EMA_fast = alpha_fast * chartPoints[ n ][ base ] + (1 - alpha_fast) * EMA_fast;
      EMA_slow = alpha_slow * chartPoints[ n ][ base ] + (1 - alpha_slow) * EMA_slow;
      MACD = EMA_fast - EMA_slow;
      MACD_arr[ n ] = MACD;
      MACD_Signal = MACD_Signal + (MACD_arr[ n ] / period_signal) - (MACD_arr[ n - period_signal ] / period_signal);
      MACD_Hist = MACD - MACD_Signal;
      indicator[ 0 ].data.push( [ chartPoints[ n ].x, parseFloat( MACD.toFixed( 8 ) ) ] );
      indicator[ 1 ].data.push( [ chartPoints[ n ].x, parseFloat( MACD_Signal.toFixed( 8 ) ) ] );
      indicator[ 2 ].data.push( [ chartPoints[ n ].x, parseFloat( MACD_Hist.toFixed( 8 ) ) ] );
    }
    return indicator;
  }
  var indicatorAverageTrueRange = function( period ) {
    period = period || 14;
    period = parseInt( period );
    var indicator = {
      name  : 'ATR (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#007cc3'
    }
    var ATR = 0;
    var chartPoints = getSeries();
    if ( chartPoints.length < period + 1 ) {
      return [ indicator ];
    }
    for ( var n = 1, l = period + 1; n < l; n++ ) {
      ATR += Math.max( chartPoints[ n ][ 'high' ] - chartPoints[ n ][ 'low' ], Math.abs( chartPoints[ n ][ 'high' ] -
      chartPoints[ n - 1 ][ 'close' ] ), Math.abs( chartPoints[ n ][ 'low' ] - chartPoints[ n - 1 ][ 'close' ] ) );
    }
    ATR = ATR / period;
    indicator.data.push( [ chartPoints[ n - 1 ].x, parseFloat( ATR.toFixed( 8 ) ) ] );
    for ( n = period + 1, l = chartPoints.length; n < l; n++ ) {
      ATR = (ATR * (period - 1) +
      Math.max( chartPoints[ n ][ 'high' ] - chartPoints[ n ][ 'low' ], Math.abs( chartPoints[ n ][ 'high' ] -
      chartPoints[ n - 1 ][ 'close' ] ), Math.abs( chartPoints[ n ][ 'low' ] - chartPoints[ n - 1 ][ 'close' ] ) )) /
      period;
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( ATR.toFixed( 8 ) ) ] );
    }
    return [ indicator ];
  }
  var indicatorComodityChanelIndex = function( period ) {
    period = period || 14;
    period = parseInt( period );
    var indicator = {
      name  : 'CCI (' + period + ')',
      data  : [],
      type  : 'spline',
      color : '#007cc3'
    }
    var chartPoints = getSeries();
    if ( chartPoints.length < period ) {
      return [ indicator ];
    }
    var CCI = 0;
    var MAD = 0;
    var SMA = [];
    var cum = 0;
    var p = [];
    for ( var n = 0, l = period; n < l; n++ ) {
      p[ n ] = (chartPoints[ n ][ 'high' ] + chartPoints[ n ][ 'low' ] + chartPoints[ n ][ 'close' ]) / 3;
      cum += p[ n ];
      SMA[ n ] = cum / (n + 1);
      MAD += Math.abs( p[ n ] - SMA[ n ] ) / (n + 1);
    }
    for ( n = period, l = chartPoints.length; n < l; n++ ) {
      p[ n ] = (chartPoints[ n ][ 'high' ] + chartPoints[ n ][ 'low' ] + chartPoints[ n ][ 'close' ]) / 3;
      SMA[ n ] = SMA[ n - 1 ] + (p[ n ] / period) - (p[ n - period ] / period);
      MAD = MAD + Math.abs( (p[ n ] - SMA[ n ]) / period ) - Math.abs( (p[ n - period ] - SMA[ n - period ]) / period );
      CCI = (1 / 0.015) * (p[ n ] - SMA[ n ]) / MAD;
      indicator.data.push( [ chartPoints[ n ].x, parseFloat( NAX.market.formatQuote( CCI, chartSymbol ) ) ] );
    }
    return [ indicator ];
  }
  var indicatorAverageDirectionalMovementIndex = function( period ) {
    period = period || 14;
    period = parseInt( period );
    var indicator = [
      {
        name  : 'ADX (' + period + ')',
        data  : [],
        type  : 'spline',
        color : '#b573f5'
      }, {
        name      : '+DI (' + period + ')',
        data      : [],
        type      : 'spline',
        dashStyle : 'Dash',
        color     : '#62b600'
      }, {
        name      : '-DI (' + period + ')',
        data      : [],
        type      : 'spline',
        dashStyle : 'Dash',
        color     : '#ff4d4d'
      }
    ];
    var alpha = 2 / (period + 1);
    var ATR = 0;
    var EMA = 0;
    var ADX = 0;
    var DIplus = 0;
    var DIminus = 0;
    var EMADIplus = 0;
    var EMADIminus = 0;
    var chartPoints = getSeries();
    if ( chartPoints.length < period + 1 ) {
      return indicator;
    }
    for ( var n = 1, l = period + 1; n < l; n++ ) {
      ATR += Math.max( chartPoints[ n ][ 'high' ] - chartPoints[ n ][ 'low' ], Math.abs( chartPoints[ n ][ 'high' ] -
      chartPoints[ n - 1 ][ 'close' ] ), Math.abs( chartPoints[ n ][ 'low' ] - chartPoints[ n - 1 ][ 'close' ] ) );
    }
    ATR = ATR / period;
    for ( n = period + 1, l = chartPoints.length; n < l; n++ ) {
      ATR = (ATR * (period - 1) +
      Math.max( chartPoints[ n ][ 'high' ] - chartPoints[ n ][ 'low' ], Math.abs( chartPoints[ n ][ 'high' ] -
      chartPoints[ n - 1 ][ 'close' ] ), Math.abs( chartPoints[ n ][ 'low' ] - chartPoints[ n - 1 ][ 'close' ] ) )) /
      period;
      var upMove = chartPoints[ n ][ 'high' ] - chartPoints[ n - 1 ][ 'high' ];
      var downMove = chartPoints[ n - 1 ][ 'low' ] - chartPoints[ n ][ 'low' ];
      var DMplus = upMove > downMove ? upMove : 0;
      var DMminus = downMove > upMove ? downMove : 0;
      EMADIplus = alpha * DMplus + (1 - alpha) * EMADIplus;
      EMADIminus = alpha * DMminus + (1 - alpha) * EMADIminus;
      DIplus = EMADIplus / ATR;
      DIminus = EMADIminus / ATR;
      ADX = alpha * (Math.abs( DIplus - DIminus ) / (DIplus + DIminus)) + (1 - alpha) * ADX;
      indicator[ 0 ].data.push( [ chartPoints[ n ].x, parseFloat( (100 * ADX).toFixed( 3 ) ) ] );
      indicator[ 1 ].data.push( [ chartPoints[ n ].x, parseFloat( (100 * DIplus).toFixed( 3 ) ) ] );
      indicator[ 2 ].data.push( [ chartPoints[ n ].x, parseFloat( (100 * DIminus).toFixed( 3 ) ) ] );
    }
    return indicator;
  }
  var getIndicatorData = function( name, params ) {
    switch ( name ) {
      case 'MA':
        switch ( params.type ) {
          case 'simple':
            return indicatorSimpleMovingAverage( params.period, params.base );
          case 'exponential':
            return indicatorExponentialMovingAverage( params.period, params.base );
          case 'cumulative':
            return indicatorCumulativeMovingAverage( params.base );
          case 'weighted':
            return indicatorWeightedMovingAverage( params.period, params.base );
        }
        break;
      case 'RSI':
        return indicatorRelativeStrengthIndex( params.period );
      case 'BB':
        return indicatorBollingerBands( params.period, params.base );
      case 'MACD':
        return indicatorMovingAverageConvergenceDivergence( params.period_fast, params.period_slow, params.period_signal, params.base );
      case 'ATR':
        return indicatorAverageTrueRange( params.period );
      case 'CCI':
        return indicatorComodityChanelIndex( params.period );
      case 'ADX':
        return indicatorAverageDirectionalMovementIndex( params.period );
    }
  }
  var addIndicatorField = function() {
    var contH = chart.containerHeight - chart.yAxis[ 0 ].top -
      chart.yAxis[ indicatorWindows == 0 ? 0 : indicatorWindows + 1 ].bottom;
    var stdHeight = Math.round( (contH - (10 * (indicatorWindows + 1))) / (indicatorWindows + 3) );
    for ( var i = 0; i <= indicatorWindows + 1; i++ ) {
      if ( i == 1 ) {
        continue;
      }
      var opts = chart.yAxis[ i ].options;
      if ( i == 0 ) {
        opts.height = 2 * stdHeight;
      } else {
        opts.height = stdHeight;
        opts.top = chart.yAxis[ 0 ].top + (opts.height * i) + ((i - 1) * 10);
      }
      chart.yAxis[ i ].setOptions( opts );
    }
    chart.addAxis( {
      title    : null,
      top      : chart.yAxis[ 0 ].top + (stdHeight * 2) + (indicatorWindows * (stdHeight + 10)) + 10,
      height   : stdHeight,
      offset   : 0,
      opposite : true
    } );
    indicatorWindows++;
    chart.redraw();
  }
  this.addIndicator = function( name, params, separate, colors, data ) {
    if ( noChart || chartType == 'line' ) {
      return;
    }
    if ( separate ) {
      addIndicatorField();
    }
    data = data || getIndicatorData( name, params );
    colors = colors || [];
    var series = [];
    for ( var i in data ) {
      var d = data[ i ];
      var options = {
        type         : d.type,
        name         : d.name,
        data         : d.data,
        yAxis        : separate ? indicatorWindows + 1 : 0,
        dataGrouping : {
          units : chartTypes[ chartPeriod ]
        },
        lineWidth    : 1,
        color        : colors[ i ] ? colors[ i ] : (d.color ? d.color : null),
        dashStyle    : d.dashStyle ? d.dashStyle : 'Solid'
      };
      if ( options.type == 'areaspline' ) {
        var rgba = hexToRgb( colors[ i ] ? colors[ i ] : (d.color ? options.color : null) );
        if ( rgba ) {
          options.fillColor = {
            linearGradient : {
              x1 : 0,
              y1 : 0,
              x2 : 0,
              y2 : 1
            },
            stops          : [
              [ 0, 'rgba(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ',0.5)' ],
              [ 1, 'rgba(' + rgba.r + ',' + rgba.g + ',' + rgba.b + ',0.1)' ]
            ]
          }
        }
      }
      series.push( chart.addSeries( options ).index );
    }
    if ( charts[ chartId ].indicators == undefined ) {
      charts[ chartId ].indicators = [];
    }
    charts[ chartId ].indicators.push( {
      name     : name,
      params   : params,
      series   : series,
      separate : separate,
      colors   : colors
    } );
  };
  var updateIndicator = function( indicator ) {
    if ( noChart ) {
      return;
    }
    var data = getIndicatorData( indicator.name, indicator.params );
    for ( var n in indicator.series ) {
      var last = chart.series[ indicator.series[ n ] ].options.data[ chart.series[ indicator.series[ n ] ].options.data.length -
      1 ][ 0 ];
      for ( var no in data[ n ].data ) {
        if ( data[ n ].data[ no ][ 0 ] >= last ) {
          chart.series[ indicator.series[ n ] ].addPoint( data[ n ].data[ no ], false );
        }
      }
    }
  }
  this.updateIndicators = function( newindicators ) {
    if ( newindicators == undefined || noChart || charts[ chartId ] == undefined ) {
      return;
    }
    charts[ chartId ].indicators = newindicators;
    redrawChart();
  }
  currentDiv.on( 'click', '#chart-add-indicator', function() {
    modalsUI.showAddIndicatorModal();
  } );
  currentDiv.on( 'click', '#chart-show-indicators', function() {
    if ( noChart == false && charts[ chartId ] != undefined ) {
      modalsUI.showManageIndictors( charts[ chartId ].indicators );
    }
  } );
};