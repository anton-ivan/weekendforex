'use strict';

var indicatorClass = function () {
	var series, params, defaultLinecolor = '#FFE700';

	this.roundTo4 = function (val) {
		if (!isNaN(val)) {
			var mult = Math.pow(10, 4);
			return (Math.round(val * mult) / mult);
		} else {
			return val;
		}
	}

	this.getSeries = function () {
		var data = [],
			n, l;
		if (!series) return data;
		if (series.cropped) {
			for (n = 0, l = series.cropStart; n < l; n++) {
				data.push({
					x: series.xData[n],
					open: series.yData[n][0],
					high: series.yData[n][1],
					low: series.yData[n][2],
					close: series.yData[n][3]
				});
			}
		}
		if (series.groupedData) {
			for (n = 0, l = series.groupedData.length; n < l; n++) {
				data.push(series.groupedData[n]);
			}
		}

		l = (series.groupedData) ? series.groupedData.length : series.xData.length;
		for (n = 0; n < l; n++) {
			data.push({
				x: series.xData[n],
				open: series.yData[n][0],
				high: series.yData[n][1],
				low: series.yData[n][2],
				close: series.yData[n][3]
			});
		}
		data.sort(function (a, b) { if (a.x > b.x) return 1; else if (a.x < b.x) return -1; else return 0});
		for (n = data.length - 2; n >= 0; n--) {
			if (data[n].x === data[n + 1].x) data.splice(n, 1);
		}
		//console.log(new Date(data[data.length-1].x).toLocaleTimeString())
		return data;
	}

	this.getIndicatorData = function (settings, data, newlinecolor) {
		params = settings;
		series = data;
		defaultLinecolor = newlinecolor || '#FFE700';
		switch (params.indicator) {
			case 'MA':
				switch (params.type) {
					case 'simple':
						return this.indicatorSimpleMovingAverage(params.period, params.base);
					case 'exponential':
						return this.indicatorExponentialMovingAverage(params.period, params.base);
					case 'cumulative':
						return this.indicatorCumulativeMovingAverage(params.base);
					case 'weighted':
						return this.indicatorWeightedMovingAverage(params.period, params.base);
				}
				break;
			case 'BB':
				return this.indicatorBollingerBands(params.period, params.base);
			case 'RSI':
				return this.indicatorRelativeStrengthIndex(params.period);
			case 'ATR':
				return this.indicatorAverageTrueRange(params.period);
			case 'CCI':
				return this.indicatorComodityChanelIndex(params.period);
			case 'ADX':
				return this.indicatorAverageDirectionalMovementIndex(params.period);
			case 'MACD':
				return this.indicatorMovingAverageConvergenceDivergence(params.periodFast, params.periodSlow, params.periodSignal, params.base);
		}
	};

	this.indicatorSimpleMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				id: 'SMA' + period + base,
				name: 'SMA (' + period + ', ' + base + ')',
				data: [],
				type: 'spline',
				color: '#FFE700',
				lineWidth: 1
			},
			SMA = 0,
			chartPoints = this.getSeries();
		if (chartPoints.length < period) return [indicator];
		for (var n = 0, l = period; n < l; n++) {
			SMA += chartPoints[n][base];
		}
		SMA = SMA / period;
		for (n = period, l = chartPoints.length; n < l; n++) {
			SMA = SMA + (chartPoints[n][base] / period) - (chartPoints[n - period][base] / period);
			indicator.data.push([chartPoints[n].x, this.roundTo4(SMA)]);
		}
		return [indicator];
	}

	this.indicatorCumulativeMovingAverage = function (b) {
		var base = b || 'close',
			indicator = {
				id: 'CMA' + base,
				name: 'CMA ' + base,
				data: [],
				type: 'spline',
				color: 'FF3000',
				lineWidth: 1
			},
			chartPoints = this.getSeries();
		if (!chartPoints.length) return [indicator];
		var CMA = chartPoints[0][base];
		for (var n = 1, l = chartPoints.length; n < l; n++) {
			CMA = CMA + (chartPoints[n][base] - CMA) / n;
			indicator.data.push([chartPoints[n].x, this.roundTo4(CMA)]);
		}
		return [indicator];
	}

	this.indicatorWeightedMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				id: 'WMA' + period + base,
				name: 'WMA (' + period + ', ' + base + ')',
				data: [],
				type: 'spline',
				color: '#ff9e00',
				lineWidth: 1
			},
			points = [],
			mult = 2 / (period * (period + 1)),
			chartPoints = this.getSeries(),
			n, l;
		for (n = 0, l = period - 1; n < l; n++) {
			if (chartPoints[n] == undefined) {
				return [indicator];
			} else {
				points.push(chartPoints[n]);
			}
		}
		for (n = period, l = chartPoints.length; n < l; n++) {
			points.push(chartPoints[n]);
			var _val = 0;
			for (var i = points.length - 1; i >= 0; i--) {
				_val += (period - i) * chartPoints[n - i][base];
			}
			indicator.data.push([chartPoints[n].x, this.roundTo4(mult * _val)]);
			points.shift();
		}
		return [indicator];
	}

	this.indicatorExponentialMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				id: 'EMA' + period + base,
				name: 'EMA (' + period + ', ' + base + ')',
				data: [],
				type: 'spline',
				color: '#ff2e6c',
				lineWidth: 1
			},
			alpha = 2 / (period + 1),
			chartPoints = this.getSeries();
		if (!chartPoints.length) return [indicator];
		var EMA = chartPoints[0][base];
		indicator.data.push([chartPoints[0].x, this.roundTo4(EMA)]);
		for (var n = 1, l = chartPoints.length; n < l; n++) {
			EMA = alpha * chartPoints[n][base] + (1 - alpha) * EMA;
			indicator.data.push([chartPoints[n].x, this.roundTo4(EMA)]);
		}
		return [indicator];
	};

	this.indicatorBollingerBands = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = [
				{
					id: 'BB' + period + base,
					name: 'BB (' + period + ')',
					data: [],
					type: 'spline',
					color: '#ff1cd7',
					lineWidth: 1
				}, {
					id: 'BBhigh' + period + base,
					name: 'BB High (' + period + ')',
					data: [],
					type: 'spline',
					color: '#bd26ff',
					lineWidth: 1
				}, {
					id: 'BBlow' + period + base,
					name: 'BB Low (' + period + ')',
					data: [],
					type: 'spline',
					color: '#937aff',
					lineWidth: 1
				}
			],
			SMA = 0,
			last = [],
			chartPoints = this.getSeries();
		if (chartPoints.length < period) return indicator;
		for (var n = 0, l = period; n < l; n++) {
			SMA += chartPoints[n][base];
			last.push(chartPoints[n][base]);
		}
		SMA = SMA / period;
		for (n = period, l = chartPoints.length; n < l; n++) {
			last.shift();
			last.push(chartPoints[n][base]);
			var sigma = 0;
			for (var i = 0; i < period; i++) {
				sigma += Math.pow(last[i] - SMA, 2);
			}
			sigma = Math.sqrt(sigma / period);
			SMA = SMA + (chartPoints[n][base] / period) - (chartPoints[n - period][base] / period);
			indicator[0].data.push([chartPoints[n].x, this.roundTo4(SMA)]);
			indicator[1].data.push([chartPoints[n].x, this.roundTo4(SMA + (2 * sigma))]);
			indicator[2].data.push([chartPoints[n].x, this.roundTo4(SMA - (2 * sigma))]);
		}
		//console.log(indicator)
		return indicator;
	};

	this.indicatorRelativeStrengthIndex = function (p) {
		var period = parseInt(p) || 14,
			indicator = {
				id: 'RSI',
				name: 'RSI (' + period + ')',
				data: [],
				type: 'spline',
				color: defaultLinecolor,
				lineWidth: 1
			},
			alpha = 2 / (period + 1),
			chartPoints = this.getSeries();
		if (!chartPoints.length || chartPoints.length < 2) return [indicator];
		var EMA_U = 0,
			EMA_D = 0;
		for (var n = 1, l = chartPoints.length; n < l; n++) {
			var dif = chartPoints[n]['close'] - chartPoints[n - 1]['close'];
			if (dif > 0) {
				EMA_U = alpha * dif + (1 - alpha) * EMA_U;
				EMA_D = (1 - alpha) * EMA_D;
			} else if (dif < 0) {
				EMA_D = alpha * (-1 * dif) + (1 - alpha) * EMA_D;
				EMA_U = (1 - alpha) * EMA_U;
			} else {
				EMA_U = (1 - alpha) * EMA_U;
				EMA_D = (1 - alpha) * EMA_D;
			}
			indicator.data.push([
				chartPoints[n].x, parseFloat((EMA_D ? 100 - (100 / (1 + ( EMA_U / EMA_D))) : 100).toFixed(4))
			]);
		}
		return [indicator];
	}

	this.indicatorAverageTrueRange = function (p) {
		var period = parseInt(p) || 14,
			indicator = {
				id: 'ATR',
				name: 'ATR (' + period + ')',
				data: [],
				type: 'spline',
				color: defaultLinecolor,
				lineWidth: 1
			},
			ATR = 0,
			chartPoints = this.getSeries();
		if (chartPoints.length < period + 1) {
			return [indicator];
		}
		for (var n = 1, l = period + 1; n < l; n++) {
			ATR += Math.max(chartPoints[n]['high'] - chartPoints[n]['low'], Math.abs(chartPoints[n]['high'] -
				chartPoints[n - 1]['close']), Math.abs(chartPoints[n]['low'] - chartPoints[n - 1]['close']));
		}
		ATR = ATR / period;
		indicator.data.push([chartPoints[n - 1].x, parseFloat(ATR.toFixed(8))]);
		for (n = period + 1, l = chartPoints.length; n < l; n++) {
			ATR = (ATR * (period - 1) +
				Math.max(chartPoints[n]['high'] - chartPoints[n]['low'], Math.abs(chartPoints[n]['high'] -
					chartPoints[n - 1]['close']), Math.abs(chartPoints[n]['low'] - chartPoints[n - 1]['close']))) /
				period;
			indicator.data.push([chartPoints[n].x, parseFloat(ATR.toFixed(8))]);
		}
		return [indicator];
	}

	this.indicatorComodityChanelIndex = function (p) {
		var period = parseInt(p) || 14,
			indicator = {
				id: 'CCI',
				name: 'CCI (' + period + ')',
				data: [],
				type: 'spline',
				color: defaultLinecolor,
				lineWidth: 1
			},
			chartPoints = this.getSeries();
		if (chartPoints.length < period) return [indicator];
		var CCI = 0,
			MAD = 0,
			SMA = [],
			cum = 0,
			p = [];
		for (var n = 0, l = period; n < l; n++) {
			p[n] = (chartPoints[n]['high'] + chartPoints[n]['low'] + chartPoints[n]['close']) / 3;
			cum += p[n];
			SMA[n] = cum / (n + 1);
			MAD += Math.abs(p[n] - SMA[n]) / (n + 1);
		}
		for (n = period, l = chartPoints.length; n < l; n++) {
			p[n] = (chartPoints[n]['high'] + chartPoints[n]['low'] + chartPoints[n]['close']) / 3;
			SMA[n] = SMA[n - 1] + (p[n] / period) - (p[n - period] / period);
			MAD = MAD + Math.abs((p[n] - SMA[n]) / period) - Math.abs((p[n - period] - SMA[n - period]) / period);
			CCI = (1 / 0.015) * (p[n] - SMA[n]) / MAD;
			indicator.data.push([chartPoints[n].x, this.roundTo4(CCI)]);
		}
		return [indicator];
	}

	this.indicatorAverageDirectionalMovementIndex = function (p) {
		var period = parseInt(p) || 14,
			indicator = [
				{
					id: 'ADX',
					name: 'ADX (' + period + ')',
					data: [],
					type: 'spline',
					color: '#46ff87',
					lineWidth: 1
				}, {
					id: 'pDI',
					name: '+DI (' + period + ')',
					data: [],
					type: 'spline',
					dashStyle: 'Dash',
					color: '#37ff0a',
					lineWidth: 1
				}, {
					id: 'mDI',
					name: '-DI (' + period + ')',
					data: [],
					type: 'spline',
					dashStyle: 'Dash',
					color: '#b9ff04',
					lineWidth: 1
				}
			],
			alpha = 2 / (period + 1),
			ATR = 0,
			EMA = 0,
			ADX = 0,
			DIplus = 0,
			DIminus = 0,
			EMADIplus = 0,
			EMADIminus = 0,
			chartPoints = this.getSeries();
		if (chartPoints.length < period + 1) {
			return indicator;
		}
		for (var n = 1, l = period + 1; n < l; n++) {
			ATR += Math.max(chartPoints[n]['high'] - chartPoints[n]['low'], Math.abs(chartPoints[n]['high'] -
				chartPoints[n - 1]['close']), Math.abs(chartPoints[n]['low'] - chartPoints[n - 1]['close']));
		}
		ATR = ATR / period;
		for (n = period + 1, l = chartPoints.length; n < l; n++) {
			ATR = (ATR * (period - 1) +
				Math.max(chartPoints[n]['high'] - chartPoints[n]['low'], Math.abs(chartPoints[n]['high'] -
					chartPoints[n - 1]['close']), Math.abs(chartPoints[n]['low'] - chartPoints[n - 1]['close']))) /
				period;
			var upMove = chartPoints[n]['high'] - chartPoints[n - 1]['high'];
			var downMove = chartPoints[n - 1]['low'] - chartPoints[n]['low'];
			var DMplus = upMove > downMove ? upMove : 0;
			var DMminus = downMove > upMove ? downMove : 0;
			EMADIplus = alpha * DMplus + (1 - alpha) * EMADIplus;
			EMADIminus = alpha * DMminus + (1 - alpha) * EMADIminus;
			DIplus = EMADIplus / ATR;
			DIminus = EMADIminus / ATR;
			ADX = alpha * (Math.abs(DIplus - DIminus) / (DIplus + DIminus)) + (1 - alpha) * ADX;
			indicator[0].data.push([chartPoints[n].x, parseFloat((100 * ADX).toFixed(3))]);
			indicator[1].data.push([chartPoints[n].x, parseFloat((100 * DIplus).toFixed(3))]);
			indicator[2].data.push([chartPoints[n].x, parseFloat((100 * DIminus).toFixed(3))]);
		}
		return indicator;
	}

	this.indicatorMovingAverageConvergenceDivergence = function (periodFast, periodSlow, periodSignal, base) {
		var period_fast = periodFast || 12,
			period_slow = periodSlow || 26,
			period_signal = periodSignal || 9,
			base = base || 'close';

		period_fast = parseInt(period_fast);
		period_slow = parseInt(period_slow);
		period_signal = parseInt(period_signal);
		var indicator = [
				{
					id: 'MACD',
					name: 'MACD (' + period_fast + ',' + period_slow + ')',
					data: [],
					type: 'spline',
					color: '#007cc3'
				}, {
					id: 'MACDsignal',
					name: 'MACD Signal (' + period_fast + ',' + period_slow + ',' + period_signal + ')',
					data: [],
					type: 'spline',
					dashStyle: 'Dash',
					color: '#ff4d4d'
				}, {
					id: 'MACDHist',
					name: 'MACD Hist (' + period_fast + ',' + period_slow + ',' + period_signal + ')',
					data: [],
					type: 'column'
				}
			],
			alpha_fast = 2 / (period_fast + 1),
			alpha_slow = 2 / (period_slow + 1),
			chartPoints = this.getSeries();
		if (chartPoints.length < period_signal) return indicator;
		var EMA_fast = chartPoints[0][base],
			EMA_slow = chartPoints[0][base],
			MACD = EMA_fast - EMA_slow,
			MACD_Signal = MACD,
			MACD_arr = [MACD]
		for (var n = 1, l = period_signal; n < l; n++) {
			EMA_fast = alpha_fast * chartPoints[n][base] + (1 - alpha_fast) * EMA_fast;
			EMA_slow = alpha_slow * chartPoints[n][base] + (1 - alpha_slow) * EMA_slow;
			MACD = EMA_fast - EMA_slow;
			MACD_arr[n] = MACD;
			MACD_Signal += MACD;
		}
		MACD_Signal = MACD_Signal / period_signal;
		var MACD_Hist = MACD - MACD_Signal;
		indicator[0].data.push([chartPoints[0].x, parseFloat(MACD.toFixed(8))]);
		indicator[1].data.push([chartPoints[0].x, parseFloat(MACD_Signal.toFixed(8))]);
		indicator[2].data.push([chartPoints[0].x, parseFloat(MACD_Hist.toFixed(8))]);
		for (n = period_signal, l = chartPoints.length; n < l; n++) {
			EMA_fast = alpha_fast * chartPoints[n][base] + (1 - alpha_fast) * EMA_fast;
			EMA_slow = alpha_slow * chartPoints[n][base] + (1 - alpha_slow) * EMA_slow;
			MACD = EMA_fast - EMA_slow;
			MACD_arr[n] = MACD;
			MACD_Signal = MACD_Signal + (MACD_arr[n] / period_signal) - (MACD_arr[n - period_signal] / period_signal);
			MACD_Hist = MACD - MACD_Signal;
			indicator[0].data.push([chartPoints[n].x, parseFloat(MACD.toFixed(8))]);
			indicator[1].data.push([chartPoints[n].x, parseFloat(MACD_Signal.toFixed(8))]);
			indicator[2].data.push([chartPoints[n].x, parseFloat(MACD_Hist.toFixed(8))]);
		}
		return indicator;
	}
}

export default new indicatorClass();
