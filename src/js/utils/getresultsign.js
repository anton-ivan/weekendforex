'use strict';

export default function getResultSign(symbol, n) {
	if(!n) return 0;
	switch (symbol) {
		case 'REDBLACK':
			if (n === 0) {
				return 0;
			} else if (n < 11) {
				return n & 1 ? -1 : 1;
			} else if (n < 19) {
				return n & 1 ? 1 : -1;
			} else if (n < 29) {
				return n & 1 ? -1 : 1;
			} else {
				return n & 1 ? 1 : -1;
			}
			break;
		case 'ODDEVEN':
			if (n === 0) return 0;
			if (n & 1) return -1;
			return 1;
			break;
		case
		'HIGHLOW':
			if (n === 0) return 0;
			if (n > 18) return 1;
			return -1;
			break;
	}
	return false;
}
