series.yAxis.removePlotLine( 999 );
series.yAxis.addPlotLine( {
    value     : f_bid,
    width     : 1,
    id        : 999,
    color     : '#c0c0c0',
    dashStyle : 'Solid',
    label     : {
        style : {
            color    : '#666',
            fontSize : '10px'
        },
        text  : NAX.market.formatQuote( f_bid, chartSymbol ),
        align : 'right',
        y     : 3,
        x     : 47
    }
} );